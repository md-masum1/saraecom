﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain._Common;
using Domain.Entities.Admin.Setup;

namespace Domain.Entities.Admin.Product
{
    public class ProductSize : AuditableEntity
    {
        [Column(Order = 1)]
        public int Id { get; set; }
        [Column(Order = 2)]
        public int? Stock { get; set; }
        [Column(Order = 3)]
        public float? Price { get; set; }

        public virtual Product Product { get; set; }
        [Column(Order = 4)]
        public int ProductId { get; set; }

        public virtual Color Color { get; set; }
        [Column(Order = 5)]
        public int? ColorId { get; set; }

        public virtual Size Size { get; set; }
        [Column(Order = 6)]
        public int SizeId { get; set; }
    }
}

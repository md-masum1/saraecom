﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain._Common;
using Microsoft.AspNetCore.Http;

namespace Domain.Entities.Admin.Product
{
    public class ProductImage : AuditableEntity
    {
        public int Id { get; set; }
        public string Image { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }

        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Admin.Setup;

namespace Domain.Entities.Admin.Product
{
    public class ProductTag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public virtual Tag Tag { get; set; }
        public int TagId { get; set; }

        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
    }
}

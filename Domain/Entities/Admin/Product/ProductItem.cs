﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace Domain.Entities.Admin.Product
{
    public class ProductItem
    {
        public int Id { get; set; }
        public int WhProductId { get; set; }
        public int ItemId { get; set; }
        public string Barcode { get; set; }
        public string ItemName { get; set; }
        public string Images { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public bool IsActive { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public string SizeName { get; set; }
        public string FabricName { get; set; }

        public byte? Stock { get; set; } = 0;

        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
    }
}

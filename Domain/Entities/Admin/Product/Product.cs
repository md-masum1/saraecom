﻿using System.Collections.Generic;
using Domain._Common;
using Domain.Entities.Admin.Setup;

namespace Domain.Entities.Admin.Product
{
    public class Product : AuditableEntity
    {
        public int Id { get; set; }
        public int? WhProductId { get; set; }
        public string Name { get; set; }
        public string ProductStyle { get; set; }
        public bool IsActive { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Fabric { get; set; }

        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }

        public virtual SubCategory SubCategory { get; set; }
        public int SubCategoryId { get; set; }

        public virtual ICollection<ProductColor> ProductColors { get; set; }
        public virtual ICollection<ProductSize> ProductSizes { get; set; }
        public virtual ICollection<ProductTag> ProductTags { get; set; }
        public virtual ICollection<ProductImage> Pictures { get; set; }

        public bool? New { get; set; }
        public bool? Sale { get; set; }
        public float? Price { get; set; }
        public float? SalePrice { get; set; }
        public float? Discount { get; set; }
        public byte? Stock { get; set; }

        public virtual ICollection<ProductItem> ProductItem { get; set; }
    }
}

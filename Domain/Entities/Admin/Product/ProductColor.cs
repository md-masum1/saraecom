﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain._Common;
using Domain.Entities.Admin.Setup;
using Microsoft.AspNetCore.Http;

namespace Domain.Entities.Admin.Product
{
    public class ProductColor : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Images { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }

        public virtual Color Color { get; set; }
        public int ColorId { get; set; }

        public virtual Product Product { get; set; }
        public int ProductId { get; set; }
    }
}

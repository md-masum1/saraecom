﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain._Common;
using Domain.Entities.Admin.Product;

namespace Domain.Entities.Admin.Setup
{
    public class Tag : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<ProductTag> ProductTags { get; set; }
    }
}

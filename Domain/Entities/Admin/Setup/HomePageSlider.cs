﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain._Common;
using Microsoft.AspNetCore.Http;

namespace Domain.Entities.Admin.Setup
{
    public class HomePageSlider : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavigateUrl { get; set; }
        [Required]
        public byte DisplayOrder { get; set; }
        public string ImagePath { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain._Common;

namespace Domain.Entities.Admin.Setup.Menus
{
    public class SubMenu : AuditableEntity
    {
        public int Id { get; set; }
        [Range(0, 100)]
        public byte DisplayOrder { get; set; }

        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }

        public virtual Menu Menu { get; set; }
        public int MenuId { get; set; }

        public virtual ICollection<SubSubMenu> SubSubMenus { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain._Common;

namespace Domain.Entities.Admin.Setup
{
    public class Category : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}

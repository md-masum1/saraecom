﻿using System.ComponentModel.DataAnnotations;
using Domain._Common;

namespace Domain.Entities.Admin.Setup
{
    public class SubCategory : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }
    }
}

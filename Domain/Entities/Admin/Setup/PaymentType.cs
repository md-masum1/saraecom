﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain._Common;
using Microsoft.AspNetCore.Http;

namespace Domain.Entities.Admin.Setup
{
    public class PaymentType : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string ImageUrl { get; set; }
        public string ImagePath { get; set; }
        [Required]
        public byte DisplayOrder { get; set; }
    }
}

﻿using System;

namespace Domain._Common
{
    public class AuditableEntity
    {
        public string CreatedBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}

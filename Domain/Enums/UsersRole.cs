﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum UsersRole
    {
        Admin,
        Customer,
        Employee
    }
}

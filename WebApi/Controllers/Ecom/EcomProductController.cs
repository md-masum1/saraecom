﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Admin.Products;
using Application.Ecom.EcomProduct;
using Application.Ecom.EcomProduct.Queries;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Common;

namespace WebApi.Controllers.Ecom
{
    [AllowAnonymous]
    [Route("api/ecom/[controller]")]
    [ApiController]
    public class EcomProductController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public EcomProductController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<IActionResult> GetProducts([FromQuery]EcomProductParams productParams)
        {
            var products = await _mediator.Send(new GetEcomProductQuery { ProductParams = productParams });

            Response.AddPagination(products.CurrentPage, products.PageSize, products.TotalCount, products.TotalPages);

            var productToReturn = _mapper.Map<IEnumerable<EcomProductToReturnDto>>(products);

            return Ok(productToReturn.ToList());
        }
    }
}
﻿using System.Threading.Tasks;
using Application.Ecom.Home.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Ecom
{
    [AllowAnonymous]
    [Route("api/ecom/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("menus")]
        public async Task<IActionResult> GetMenus()
        {
            return Ok(await _mediator.Send(new GetMenusQuery()));
        }

        [HttpGet("sliders")]
        public async Task<IActionResult> GetSliders()
        {
            return Ok(await _mediator.Send(new GetSlidersQuery()));
        }

        [HttpGet("banners")]
        public async Task<IActionResult> GetBanners()
        {
            return Ok(await _mediator.Send(new GetBannersQuery()));
        }
    }
}
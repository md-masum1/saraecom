﻿using System.Linq;
using System.Threading.Tasks;
using Application._Common.Paging;
using Application.Admin.Admin.Commands;
using Application.Admin.Admin.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApi.Common;

namespace WebApi.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IMediator _mediator;
        public AdminController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("userWithRoles")]
        public async Task<IActionResult> GetUserWithRoles()
        {
            return Ok(await _mediator.Send(new GetUserWithRolesQuery()));
        }

        [HttpGet("roles")]
        public async Task<IActionResult> GetAllRoles()
        {
            return Ok(await _mediator.Send(new GetAllRolesQuery()));
        }

        [HttpPost("editRoles/{userName}")]
        public async Task<IActionResult> EditRoles(string userName, EditRolesCommand command)
        {
            if (string.IsNullOrWhiteSpace(userName))
                return BadRequest();

            command.UserName = userName;

            var result = await _mediator.Send(command);

            if (result.result.Errors.Any())
                return BadRequest(result.result.Errors);

            return Ok(result.roles);
        }

        [HttpGet("users")]
        public async Task<IActionResult> GetAllUsers([FromQuery]UserParams userParams)
        {
            var users = await _mediator.Send(new GetAllUsersQuery { UserParams = userParams });

            Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);
            return Ok(users.ToList());
        }

        [HttpPost("createUser")]
        public async Task<IActionResult> CreateUser(CreateUserCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.result.Succeeded)
                return Ok(result.user);

            return BadRequest(result.result.Errors);
        }

        [HttpPost("deleteUser/{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var result = await _mediator.Send(new DeleteUserCommand { UserId = userId });

            if (result.Succeeded)
                return Ok();

            return BadRequest(result.Errors);
        }
    }
}
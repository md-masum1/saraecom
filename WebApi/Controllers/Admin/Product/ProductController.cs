﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Admin.Products;
using Application.Admin.Products.Commands;
using Application.Admin.Products.Commands.CreateCommands;
using Application.Admin.Products.Commands.DeleteCommands;
using Application.Admin.Products.Queries;
using AutoMapper;
using Infrastructure.Identity;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApi.Common;

namespace WebApi.Controllers.Admin.Product
{
    [ServiceFilter(typeof(AuthorizationFilter))]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public ProductController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet("getProduct")]
        public async Task<IActionResult> GetProduct([FromQuery]ProductParams productParams)
        {
            var products = await _mediator.Send(new GetProductQuery { ProductParams = productParams });

            Response.AddPagination(products.CurrentPage, products.PageSize, products.TotalCount, products.TotalPages);

            var productToReturn = _mapper.Map<IEnumerable<ProductToReturnDto>>(products);

            return Ok(productToReturn.ToList());
        }

        [HttpGet("getProduct/{style}")]
        public async Task<IActionResult> GetProductByStyle(string style)
        {
            var product = await _mediator.Send(new GetProductByStyleQuery {Style = style.Trim()});

            return Ok(product);
        }

        [HttpPost("getProductFromPos")]
        public async Task<IActionResult> GetProductFromPos()
        {
            var result = await _mediator.Send(new GetProductFromPosCommand());

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpGet("GetProductPhotos/{id}")]
        public async Task<IActionResult> GetProductPhotos(int id)
        {
            var photos = await _mediator.Send(new GetProductPhotosQuery { ProductId = id });

            return Ok(photos);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateProduct(UpdateProductCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpPost("updateItem")]
        public async Task<IActionResult> UpdateProductItem([FromForm]UpdateProductItemCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }



        [HttpPost("createProduct")]
        public async Task<IActionResult> CreateProduct(CreateProductCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpPost("UploadImage/{id}")]
        public async Task<IActionResult> UploadProductImage(int id, [FromForm]UploadProductImageCommand command)
        {
            command.Id = id;
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("deleteImage/{id}/{productId}")]
        public async Task<IActionResult> DeleteItemImage(int id, int productId)
        {
            var result = await _mediator.Send(new DeleteProductImageCommand { ImageId = id, ProductId = productId });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }

        [HttpGet("getProductColor/{productId}")]
        public async Task<IActionResult> GetProductColor(int productId)
        {
            var productColor = await _mediator.Send(new GetProductColorQuery{ProductId = productId});
            return Ok(productColor);
        }

        [HttpPost("AddProductColor")]
        public async Task<IActionResult> AddProductColor([FromForm]AddProductColorCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("deleteProductColor/{colorId}/{productId}")]
        public async Task<IActionResult> DeleteProductColor(int colorId, int productId)
        {
            var result = await _mediator.Send(new DeleteProductColorCommand { ColorId = colorId, ProductId = productId });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }

        [HttpGet("getProductSize/{productId}")]
        public async Task<IActionResult> GetProductSize(int productId)
        {
            var productSize = await _mediator.Send(new GetProductSizeQuery { ProductId = productId });
            return Ok(productSize);
        }

        [HttpPost("AddProductSize")]
        public async Task<IActionResult> AddProductSize(AddProductSizeCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("deleteProductSize/{id}")]
        public async Task<IActionResult> DeleteProductSize(int id)
        {
            var result = await _mediator.Send(new DeleteProductSizeCommand { Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
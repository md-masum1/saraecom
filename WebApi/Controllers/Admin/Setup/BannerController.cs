﻿using System.Threading.Tasks;
using Application.Admin.Setup.HomePageBanners.Commands;
using Application.Admin.Setup.HomePageBanners.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class BannerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BannerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetBanners()
        {
            return Ok(await _mediator.Send(new GetBannersQuery()));
        }

        [HttpPost]
        public async Task<IActionResult> PostBanner([FromForm]CreateBannerCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBanner(int id)
        {
            var result = await _mediator.Send(new DeleteBannerCommand { Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
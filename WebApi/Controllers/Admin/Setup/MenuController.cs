﻿using System.Threading.Tasks;
using Application.Admin.Setup.Menus.Commands.CreateCommand;
using Application.Admin.Setup.Menus.Commands.DeleteCommand;
using Application.Admin.Setup.Menus.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MenuController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("getMenu")]
        public async Task<IActionResult> GetMenus()
        {
            var result = await _mediator.Send(new GetMenusQuery());
            return Ok(result);
        }

        [HttpGet("getSubMenu/{id}")]
        public async Task<IActionResult> GetSubMenus(int id)
        {
            var result = await _mediator.Send(new GetSubMenusQuery { MenuId = id });
            return Ok(result);
        }

        [HttpGet("getSubSubMenu/{subMenuId}")]
        public async Task<IActionResult> GetSubSubMenus(int subMenuId)
        {
            var result = await _mediator.Send(new GetSubSubMenusQuery { SubMenuId = subMenuId });
            return Ok(result);
        }

        [HttpPost("createMenu")]
        public async Task<IActionResult> CreateMenu(CreateMenuCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest();
        }

        [HttpPost("createSubMenu")]
        public async Task<IActionResult> CreateSubMenu(CreateSubMenuCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest();
        }

        [HttpPost("createSubSubMenu")]
        public async Task<IActionResult> CreateSubSubMenu(CreateSubSubMenuCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest();
        }

        [HttpDelete("deleteMenu/{id}")]
        public async Task<IActionResult> DeleteMenu(int id)
        {
            var result = await _mediator.Send(new DeleteMenuCommand { Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest();
        }

        [HttpDelete("deleteSubMenu/{id}/{menuId}")]
        public async Task<IActionResult> DeleteSubMenu(int id, int menuId)
        {
            var result = await _mediator.Send(new DeleteSubMenuCommand { Id = id, MenuId = menuId });

            if (result.Succeeded)
                return NoContent();

            return BadRequest();
        }

        [HttpDelete("deleteSubSubMenu/{id}/{subMenuId}")]
        public async Task<IActionResult> DeleteSubSubMenu(int id, int subMenuId)
        {
            var result = await _mediator.Send(new DeleteSubSubMenuCommand { Id = id, SubMenuId = subMenuId });

            if (result.Succeeded)
                return NoContent();

            return BadRequest();
        }
    }
}
﻿using System.Threading.Tasks;
using Application.Admin.Setup.Colors.Commands;
using Application.Admin.Setup.Colors.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class ColorController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ColorController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetColor()
        {
            return Ok(await _mediator.Send(new GetColorsQuery()));
        }

        [HttpPost]
        public async Task<IActionResult> CreateColor(CreateColorCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColor(int id)
        {
            var result = await _mediator.Send(new DeleteColorCommand{Id = id});

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
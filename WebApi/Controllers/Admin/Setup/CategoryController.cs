﻿using System.Threading.Tasks;
using Application.Admin.Setup.Categories.Commands;
using Application.Admin.Setup.Categories.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategory()
        {
            return Ok(await _mediator.Send(new GetCategoriesQuery()));
        }

        [HttpPost("createCategory")]
        public async Task<IActionResult> CreateCategory(CreateCategoryCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpPost("createSubCategory")]
        public async Task<IActionResult> CreateSubCategory(CreateSubCategoryCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("deleteCategory/{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var result = await _mediator.Send(new DeleteCategoryCommand{Id = id});

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }

        [HttpDelete("deleteSubCategory/{id}")]
        public async Task<IActionResult> DeleteSubCategory(int id)
        {
            var result = await _mediator.Send(new DeleteSubCategoryCommand{Id = id});

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
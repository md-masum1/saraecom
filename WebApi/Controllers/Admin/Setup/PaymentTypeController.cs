﻿using System.Threading.Tasks;
using Application.Admin.Setup.PaymentTypes.Commands;
using Application.Admin.Setup.PaymentTypes.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class PaymentTypeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PaymentTypeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaymentTypes()
        {
            var result = await _mediator.Send(new GetPaymentTypesQuery());
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentType([FromForm]CreatePaymentTypeCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdatePaymentType([FromForm]UpdatePaymentTypeCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePaymentType(int id)
        {
            var result = await _mediator.Send(new DeletePaymentTypeCommand { Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
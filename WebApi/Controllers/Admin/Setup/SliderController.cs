﻿using System.Threading.Tasks;
using Application.Admin.Setup.HomePageSliders.Commands;
using Application.Admin.Setup.HomePageSliders.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class SliderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SliderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetSliders()
        {
            return Ok(await _mediator.Send(new GetSlidersQuery()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSlider(int id)
        {
            return Ok(await _mediator.Send(new GetSliderQuery { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> PostSlider([FromForm]CreateSliderCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSlider(int id)
        {
            var result = await _mediator.Send(new DeleteSliderCommand { Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
﻿using System.Threading.Tasks;
using Application.Admin.Setup.Tags.Commands;
using Application.Admin.Setup.Tags.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TagController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetTags()
        {
            return Ok(await _mediator.Send(new GetTagsQuery()));
        }

        [HttpPost]
        public async Task<IActionResult> CreateTag(CreateTagCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTag(int id)
        {
            var result = await _mediator.Send(new DeleteTagCommand {Id = id});

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
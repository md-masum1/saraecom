﻿using System.Threading.Tasks;
using Application.Admin.Setup.Sizes.Commands;
using Application.Admin.Setup.Sizes.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Admin.Setup
{
    [Route("api/setup/[controller]")]
    [ApiController]
    public class SizeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SizeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetSize()
        {
            return Ok(await _mediator.Send(new GetSizesQuery()));
        }

        [HttpPost]
        public async Task<IActionResult> CreateSize(CreateSizeCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest(result.Errors);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSize(int id)
        {
            var result = await _mediator.Send(new DeleteSizeCommand{ Id = id });

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }
    }
}
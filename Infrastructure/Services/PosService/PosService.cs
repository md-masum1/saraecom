﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using Domain.Entities.Admin.Product;
using Infrastructure.Persistence;

namespace Infrastructure.Services.PosService
{
    public class PosService : IPosService
    {
        public async Task<IList<Product>> GetProductsFromWarehouse()
        {
            return await ExternalApiContext<IList<Product>>.GetAsync(ServiceConfiguration.PosServerUrl, "ecom/Product/getProducts");
        }

        public async Task LoadWarehouseProduct()
        {
            await ExternalApiContext<IList<string>>.GetAsync(ServiceConfiguration.PosServerUrl, "ecom/Product/loadData");
        }

        public async Task<bool> UpdateWarehouseProduct(int whProductId)
        {
            var result = await ExternalApiContext<string>.GetAsync(ServiceConfiguration.PosServerUrl, "ecom/Product/updateProduct?productId=" + whProductId);

            if (result == "Update Successfully")
            {
                return true;
            }

            return false;
        }
    }
}

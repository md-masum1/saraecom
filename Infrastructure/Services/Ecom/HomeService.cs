﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Ecom.Home;
using Domain.Entities.Admin.Setup;
using Domain.Entities.Admin.Setup.Menus;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Ecom
{
    public class HomeService : IHomeService
    {
        private readonly ApplicationDbContext _context;

        public HomeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<IList<HomePageBanner>> GetBanners()
        {
            return await _context.HomePageBanners.OrderBy(c => c.DisplayOrder).ToListAsync();
        }

        public async Task<IList<Menu>> GetMenus()
        {
            return await _context.Menus.OrderBy(c => c.DisplayOrder).ToListAsync();
        }

        public async Task<IList<HomePageSlider>> GetSliders()
        {
            return await _context.HomePageSliders.OrderBy(c => c.DisplayOrder).ToListAsync();
        }
    }
}

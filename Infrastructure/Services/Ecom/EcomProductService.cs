﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application._Common.Paging;
using Application.Ecom.EcomProduct;
using Domain.Entities.Admin.Product;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using MoreLinq.Extensions;

namespace Infrastructure.Services.Ecom
{
    public class EcomProductService : IEcomProductService
    {
        private readonly ApplicationDbContext _context;

        public EcomProductService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<PagedList<Product>> GetProducts(EcomProductParams productParams)
        {
            var products = _context.Products.Where(p => p.IsActive).AsQueryable();

            return await PagedList<Product>.CreateAsync(products, productParams.PageNumber, productParams.PageSize);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.Categories;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext _context;

        public CategoryService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<IList<Category>> GetCategories()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<Result> CreateCategory(Category category)
        {
            var categoryToCreate =
                await _context.Categories.FirstOrDefaultAsync(c => c.Name.ToLower() == category.Name.ToLower().Trim());

            if (categoryToCreate != null)
                return Result.Failure(new List<string> { "category already exist" });

            if (category.Id > 0)
            {
                var categoryToUpdate = await _context.Categories.FirstOrDefaultAsync(c => c.Id == category.Id);

                if (categoryToUpdate != null)
                {
                    categoryToUpdate.Name = category.Name;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create category" });
                }
                return Result.Failure(new List<string> { "category not found" });
            }
            else
            {
                await _context.AddAsync(category);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't create category" });

            }
        }

        public async Task<Result> CreateSubCategory(SubCategory subCategory)
        {
            var subCategoryToCreate =
                await _context.SubCategories.FirstOrDefaultAsync(c => c.Name.ToLower() == subCategory.Name.ToLower().Trim());

            if (subCategoryToCreate != null)
                return Result.Failure(new List<string> { "sub category already exist" });

            if (subCategory.Id > 0)
            {
                var categoryToUpdate = await _context.SubCategories.FirstOrDefaultAsync(c => c.Id == subCategory.Id);

                if (categoryToUpdate != null)
                {
                    categoryToUpdate.Name = subCategory.Name;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create sub category" });
                }
                return Result.Failure(new List<string> { "sub category not found" });
            }
            else
            {
                await _context.AddAsync(subCategory);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't create sub category" });

            }
        }

        public async Task<Result> DeleteCategory(int id)
        {
            var categoryToDelete = await _context.Categories.FirstOrDefaultAsync(c => c.Id == id);

            if (categoryToDelete != null)
            {
                _context.Remove(categoryToDelete);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete category" });
            }

            return Result.Failure(new List<string> { "category not found" });
        }

        public async Task<Result> DeleteSubCategory(int id)
        {
            var subCategoryToDelete = await _context.SubCategories.FirstOrDefaultAsync(c => c.Id == id);

            if (subCategoryToDelete != null)
            {
                _context.Remove(subCategoryToDelete);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete sub category" });
            }

            return Result.Failure(new List<string> { "sub category not found" });
        }
    }
}

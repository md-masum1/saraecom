﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.Tags;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class TagService : ITagService
    {
        private readonly ApplicationDbContext _context;

        public TagService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<Result> CreateTag(Tag tag)
        {
            var tagToCreate =
                await _context.Tags.FirstOrDefaultAsync(c => c.Name.ToLower() == tag.Name.ToLower().Trim());

            if (tagToCreate != null)
            {
                return Result.Failure(new List<string> { "tag already exist" });
            }

            if (tag.Id > 0)
            {
                var tagToUpdate = await _context.Tags.FirstOrDefaultAsync(c => c.Id == tag.Id);

                if (tagToUpdate != null)
                {
                    tagToUpdate.Name = tag.Name;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create tag" });
                }
                return Result.Failure(new List<string> { "tag not found" });
            }
            else
            {
                await _context.AddAsync(tag);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't create tag" });

            }
        }

        public async Task<Result> DeleteTag(int id)
        {
            var tagToDelete = await _context.Tags.FirstOrDefaultAsync(c => c.Id == id);

            if (tagToDelete != null)
            {
                _context.Remove(tagToDelete);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete tag" });
            }

            return Result.Failure(new List<string> { "tag not found" });
        }

        public async Task<IList<Tag>> GetTags()
        {
            return await _context.Tags.ToListAsync();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.Sizes;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class SizeService : ISizeService
    {
        private readonly ApplicationDbContext _context;

        public SizeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<Result> CreateSize(Size size)
        {
            var sizeToCreate =
                await _context.Sizes.FirstOrDefaultAsync(c => c.Name.ToLower() == size.Name.ToLower().Trim());

            if (sizeToCreate != null)
            {
                return Result.Failure(new List<string> { "size already exist" });
            }

            if (size.Id > 0)
            {
                var sizeToUpdate = await _context.Sizes.FirstOrDefaultAsync(c => c.Id == size.Id);

                if (sizeToUpdate != null)
                {
                    sizeToUpdate.Name = size.Name;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create size" });
                }
                return Result.Failure(new List<string> { "size not found" });
            }
            else
            {
                await _context.AddAsync(size);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't create size" });

            }
        }

        public async Task<Result> DeleteSize(int id)
        {
            var sizeToDelete = await _context.Sizes.FirstOrDefaultAsync(c => c.Id == id);

            if (sizeToDelete != null)
            {
                _context.Remove(sizeToDelete);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete size" });
            }

            return Result.Failure(new List<string> { "size not found" });
        }

        public async Task<IList<Size>> GetSizes()
        {
            return await _context.Sizes.ToListAsync();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.HomePageSliders;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Files;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class SliderService : ISliderService
    {
        private readonly ApplicationDbContext _context;
        private readonly FileProcessor _fileProcessor;

        public SliderService(ApplicationDbContext context, FileProcessor fileProcessor)
        {
            _context = context;
            _fileProcessor = fileProcessor;
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<Result> CreateSlider(HomePageSlider homePageSlider)
        {
            if (homePageSlider.Id > 0)
            {
                var sliderToUpdate = await _context.HomePageSliders.FirstOrDefaultAsync(c => c.Id == homePageSlider.Id);
                if (sliderToUpdate != null)
                {
                    if (homePageSlider.ImageFile != null)
                    {
                        _fileProcessor.DeleteFile(homePageSlider.ImagePath);
                        var image = await _fileProcessor.SaveImage(homePageSlider.ImageFile, "\\Files\\HomePageSlider\\");
                        sliderToUpdate.ImagePath = image.FilePath;
                    }

                    sliderToUpdate.Title = homePageSlider.Title;
                    sliderToUpdate.Description = homePageSlider.Description;
                    sliderToUpdate.NavigateUrl = homePageSlider.NavigateUrl;
                    sliderToUpdate.DisplayOrder = homePageSlider.DisplayOrder;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't update slider" });
                }
                return Result.Failure(new List<string> { "Slider not found" });
            }
            else
            {
                if (homePageSlider.ImageFile != null)
                {
                    var image = await _fileProcessor.SaveImage(homePageSlider.ImageFile, "\\Files\\HomePageSlider\\");
                    homePageSlider.ImagePath = image.FilePath;

                    await _context.AddAsync(homePageSlider);

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create slider" });

                }
                return Result.Failure(new List<string> { "Image is required" });
            }
        }

        public async Task<Result> DeleteSlider(int id)
        {
            var sliderToDelete = await _context.HomePageSliders.FirstOrDefaultAsync(s => s.Id == id);

            if (sliderToDelete != null)
            {
                _context.Remove(sliderToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete slider" });
            }

            return Result.Failure(new List<string> { "Slider not found" });
        }



        public async Task<HomePageSlider> GetSlider(int id)
        {
            return await _context.HomePageSliders.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IList<HomePageSlider>> GetSliders()
        {
            return await _context.HomePageSliders.ToListAsync();
        }
    }
}

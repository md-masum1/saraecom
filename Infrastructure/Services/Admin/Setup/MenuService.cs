﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.Menus;
using Domain.Entities.Admin.Setup.Menus;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class MenuService : IMenuService
    {
        private readonly ApplicationDbContext _context;

        public MenuService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<List<Menu>> GetAllMenus()
        {
            return await _context.Menus.ToListAsync();
        }

        public async Task<List<SubMenu>> GetAllSubMenus(int menuId)
        {
            return await _context.SubMenus.Where(s => s.MenuId == menuId).ToListAsync();
        }

        public async Task<List<SubSubMenu>> GetAllSubSubMenus(int subMenuId)
        {
            return await _context.SubSubMenus.Where(s => s.SubMenuId == subMenuId).ToListAsync();
        }

        public async Task<Result> SaveMenu(Menu menu)
        {
            var menuToSave = await _context.Menus.FirstOrDefaultAsync(c => c.Id == menu.Id);

            if (menuToSave != null)
            {
                menuToSave.DisplayOrder = menu.DisplayOrder;
                menuToSave.Path = menu.Path;
                menuToSave.Title = menu.Title;
                menuToSave.Type = menu.Type;
                menuToSave.MegaMenu = menu.MegaMenu;
                menuToSave.MegaMenuType = menu.MegaMenuType;

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to update menu" });
            }
            else
            {
                await _context.Menus.AddAsync(menu);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to save menu" });
            }
        }

        public async Task<Result> SaveSubMenu(SubMenu subMenu)
        {
            var subMenuToSave = await _context.SubMenus.FirstOrDefaultAsync(c => c.Id == subMenu.Id);

            if (subMenuToSave != null)
            {
                subMenuToSave.DisplayOrder = subMenu.DisplayOrder;
                subMenuToSave.Path = subMenu.Path;
                subMenuToSave.Title = subMenu.Title;
                subMenuToSave.Type = subMenu.Type;
                subMenuToSave.MegaMenu = subMenu.MegaMenu;
                subMenuToSave.MegaMenuType = subMenu.MegaMenuType;

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to update Sub menu" });
            }
            else
            {
                await _context.SubMenus.AddAsync(subMenu);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to save menu" });
            }
        }

        public async Task<Result> SaveSubSubMenu(SubSubMenu subSubMenu)
        {
            var subSubMenuToSave = await _context.SubSubMenus.FirstOrDefaultAsync(c => c.Id == subSubMenu.Id);

            if (subSubMenuToSave != null)
            {
                subSubMenuToSave.DisplayOrder = subSubMenu.DisplayOrder;
                subSubMenuToSave.Path = subSubMenu.Path;
                subSubMenuToSave.Title = subSubMenu.Title;
                subSubMenuToSave.Type = subSubMenu.Type;
                subSubMenuToSave.MegaMenu = subSubMenu.MegaMenu;
                subSubMenuToSave.MegaMenuType = subSubMenu.MegaMenuType;

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to update Sub menu" });
            }
            else
            {
                await _context.SubSubMenus.AddAsync(subSubMenu);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Failed to save menu" });
            }
        }

        public async Task<Result> DeleteMenu(int menuId)
        {
            var menuToDelete = await _context.Menus.FirstOrDefaultAsync(c => c.Id == menuId);

            if (menuToDelete != null)
            {
                _context.Menus.Remove(menuToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }
            return Result.Failure(new List<string> { "Failed to delete menu" });
        }

        public async Task<Result> DeleteSubMenu(int menuId, int subMenuId)
        {
            var subMenuToDelete = await _context.SubMenus.FirstOrDefaultAsync(c => c.Id == subMenuId && c.MenuId == menuId);

            if (subMenuToDelete != null)
            {
                _context.SubMenus.Remove(subMenuToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }
            return Result.Failure(new List<string> { "Failed to delete sub menu" });
        }

        public async Task<Result> DeleteSubSubMenu(int subMenuId, int subSubMenuId)
        {
            var subSubMenuToDelete = await _context.SubSubMenus.FirstOrDefaultAsync(c => c.Id == subSubMenuId && c.SubMenuId == subMenuId);

            if (subSubMenuToDelete != null)
            {
                _context.SubSubMenus.Remove(subSubMenuToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }
            return Result.Failure(new List<string> { "Failed to delete sub sub menu" });
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

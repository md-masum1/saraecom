﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.PaymentTypes;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Files;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class PaymentTypeService : IPaymentTypeService
    {
        private readonly ApplicationDbContext _context;
        private readonly FileProcessor _fileProcessor;

        public PaymentTypeService(ApplicationDbContext context, FileProcessor fileProcessor)
        {
            _context = context;
            _fileProcessor = fileProcessor;
        }

        public async Task<Result> CreatePaymentType(PaymentType paymentType)
        {
            var paymentTypeName = await _context.PaymentTypes.FirstOrDefaultAsync(c => c.Name == paymentType.Name.Trim());

            if (paymentTypeName != null)
                return Result.Failure(new List<string> { "Name already exist" });

            var paymentTypeToSave = await _context.PaymentTypes.FirstOrDefaultAsync(c => c.Id == paymentType.Id);

            if (paymentTypeToSave == null)
            {
                var image = await _fileProcessor.SaveImage(paymentType.ImageFile, "\\Files\\PaymentType\\");
                paymentType.ImagePath = image.FilePath;
                paymentType.Name = paymentType.Name.Trim();

                await _context.PaymentTypes.AddAsync(paymentType);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }
            return Result.Failure(new List<string> { "Failed to save payment type" });
        }

        public async Task<Result> UpdatePaymentType(PaymentType paymentType)
        {
            var paymentTypeToSave = await _context.PaymentTypes.FirstOrDefaultAsync(c => c.Id == paymentType.Id);

            if (paymentTypeToSave != null)
            {
                if (paymentType.ImageFile != null && paymentType.ImageFile.Length > 0)
                {
                    _fileProcessor.DeleteFile(paymentTypeToSave.ImagePath);
                    var image = await _fileProcessor.SaveImage(paymentType.ImageFile, "\\Files\\PaymentType\\");
                    paymentTypeToSave.ImagePath = image.FilePath;
                }

                paymentTypeToSave.DisplayOrder = paymentType.DisplayOrder;
                paymentTypeToSave.Name = paymentType.Name.Trim();


                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }
            return Result.Failure(new List<string> { "Failed to update payment type" });
        }

        public async Task<Result> DeletePaymentType(int id)
        {
            var paymentTypeToDelete = await _context.PaymentTypes.FirstOrDefaultAsync(c => c.Id == id);

            if (paymentTypeToDelete != null)
            {
                _context.PaymentTypes.Remove(paymentTypeToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                {
                    _fileProcessor.DeleteFile(paymentTypeToDelete.ImagePath);
                    return Result.Success();
                }

            }
            return Result.Failure(new List<string> { "Failed to delete payment type" });
        }

        public async Task<PaymentType> GetPaymentType(int id)
        {
            return await _context.PaymentTypes.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IList<PaymentType>> GetPaymentTypes()
        {
            return await _context.PaymentTypes.ToListAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

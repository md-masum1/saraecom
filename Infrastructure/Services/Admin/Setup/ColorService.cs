﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.Colors;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class ColorService : IColorService
    {
        private readonly ApplicationDbContext _context;

        public ColorService(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<Result> CreateColor(Color color)
        {
            var colorToCreate =
                await _context.Colors.FirstOrDefaultAsync(c => c.Name.ToLower() == color.Name.ToLower().Trim());

            if (color.Id > 0)
            {
                var colorToUpdate = await _context.Colors.FirstOrDefaultAsync(c => c.Id == color.Id);

                if (colorToUpdate != null)
                {
                    colorToUpdate.Name = color.Name;
                    colorToUpdate.ColorCode = color.ColorCode;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create color" });
                }
                return Result.Failure(new List<string> { "color not found" });
            }
            else
            {
                if(colorToCreate != null)
                    return Result.Failure(new List<string> { "color already exist" });

                await _context.AddAsync(color);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't create color" });

            }
        }

        public async Task<Result> DeleteColor(int id)
        {
            var colorToDelete = await _context.Colors.FirstOrDefaultAsync(c => c.Id == id);

            if (colorToDelete != null)
            {
                _context.Remove(colorToDelete);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete color" });
            }

            return Result.Failure(new List<string> { "color not found" });
        }

        public async Task<IList<Color>> GetColors()
        {
            return await _context.Colors.ToListAsync();
        }
    }
}

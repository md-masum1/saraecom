﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application.Admin.Setup.HomePageBanners;
using Domain.Entities.Admin.Setup;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Files;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Setup
{
    public class BannerService : IBannerService
    {
        private readonly ApplicationDbContext _context;
        private readonly FileProcessor _fileProcessor;

        public BannerService(ApplicationDbContext context, FileProcessor fileProcessor)
        {
            _context = context;
            _fileProcessor = fileProcessor;
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<Result> CreateBanner(HomePageBanner homePageBanner)
        {
            if (homePageBanner.Id > 0)
            {
                var bannerToUpdate = await _context.HomePageBanners.FirstOrDefaultAsync(c => c.Id == homePageBanner.Id);
                if (bannerToUpdate != null)
                {
                    if (homePageBanner.ImageFile != null)
                    {
                        _fileProcessor.DeleteFile(homePageBanner.ImagePath);
                        var image = await _fileProcessor.SaveImage(homePageBanner.ImageFile, "\\Files\\homePageBanner\\");
                        bannerToUpdate.ImagePath = image.FilePath;
                    }

                    bannerToUpdate.Title = homePageBanner.Title;
                    bannerToUpdate.Description = homePageBanner.Description;
                    bannerToUpdate.NavigateUrl = homePageBanner.NavigateUrl;
                    bannerToUpdate.DisplayOrder = homePageBanner.DisplayOrder;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't update banner" });
                }
                return Result.Failure(new List<string> { "banner not found" });
            }
            else
            {
                if (homePageBanner.ImageFile != null)
                {
                    var image = await _fileProcessor.SaveImage(homePageBanner.ImageFile, "\\Files\\homePageBanner\\");
                    homePageBanner.ImagePath = image.FilePath;

                    await _context.AddAsync(homePageBanner);

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't create banner" });

                }
                return Result.Failure(new List<string> { "Image is required" });
            }
        }

        public async Task<Result> DeleteBanner(int id)
        {
            var bannerToDelete = await _context.HomePageBanners.FirstOrDefaultAsync(s => s.Id == id);

            if (bannerToDelete != null)
            {
                _context.Remove(bannerToDelete);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't delete banner" });
            }

            return Result.Failure(new List<string> { "banner not found" });
        }

        public async Task<HomePageBanner> GetBanner(int id)
        {
            return await _context.HomePageBanners.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IList<HomePageBanner>> GetBanners()
        {
            return await _context.HomePageBanners.ToListAsync();
        }
    }
}

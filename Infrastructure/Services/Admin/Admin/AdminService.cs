﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application._Common.Models;
using Application._Common.Paging;
using Application.Admin.Admin;
using Infrastructure.Identity;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Admin
{
    public class AdminService : IAdminService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public AdminService(ApplicationDbContext context,
            UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<string> GetUserNameAsync(int userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

            return user.UserName;
        }

        public async Task<PagedList<UserToReturnDto>> GetAllUsersAsync(UserParams userParams)
        {
            var users = _userManager.Users.Select(user => new UserToReturnDto
            {
                Id = user.Id,
                UserName = user.UserName,
                EmployeeId = user.EmployeeId,
                Email = user.Email
            }).OrderByDescending(c => c.Id);

            return await PagedList<UserToReturnDto>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<UserToReturnDto> GetUserAsync(int userId)
        {
            var users = await _userManager.Users.Select(user => new UserToReturnDto
            {
                Id = user.Id,
                UserName = user.UserName,
                EmployeeId = user.EmployeeId,
                Email = user.Email
            }).FirstOrDefaultAsync(u => u.Id == userId);

            return users;
        }

        public async Task<(Result Result, int UserId)> CreateUserAsync(CreateUserDto createUserDto)
        {
            var user = new User
            {
                UserName = createUserDto.UserName,
                Email = createUserDto.Email,
                EmployeeId = createUserDto.EmployeeId,
                CreateBy = createUserDto.CreateBy,
                CreateDate = createUserDto.CreateDate
            };

            var result = await _userManager.CreateAsync(user, createUserDto.Password);

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<Result> DeleteUserAsync(int userId)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if (user != null)
            {
                return await DeleteUserAsync(user);
            }

            return Result.Success();
        }

        private async Task<Result> DeleteUserAsync(User user)
        {
            var result = await _userManager.DeleteAsync(user);

            return result.ToApplicationResult();
        }

        public async Task<object> GetUserWithRolesAsync()
        {
            var userList = await _context.Users.OrderBy(x => x.UserName).Select(user => new
            {
                user.Id,
                user.EmployeeId,
                user.UserName,
                Roles = (from userRole in user.UserRoles
                         join role in _context.Roles on userRole.RoleId equals role.Id
                         select role.Name).ToList()
            }).ToListAsync();

            return userList;
        }

        public async Task<object> GetAllRolesAsync()
        {
            var roleList = await _context.Roles.OrderBy(x => x.Name).Select(roles => new
            {
                name = roles.Name,
                value = roles.Name
            }).ToListAsync();

            return roleList;
        }

        public async Task<(Result Result, IList<string> roles)> EditRolesAsync(string userName, RoleEditDto roleEditDto)
        {
            var user = await _userManager.FindByNameAsync(userName);

            var userRoles = await _userManager.GetRolesAsync(user);

            var selectedRole = roleEditDto.RoleNames;

            selectedRole ??= new string[] { };

            var result = await _userManager.AddToRolesAsync(user, selectedRole.Except(userRoles));

            if (!result.Succeeded)
                return (Result.Failure(new List<string> { "User Already Exist" }), null);

            result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRole));

            if (!result.Succeeded)
                return (Result.Failure(new List<string> { "Failed to remove roles" }), null);

            return (Result.Success(), await _userManager.GetRolesAsync(user));
        }

        public void Dispose()
        {
            _context.Dispose();
            _userManager.Dispose();
        }
    }
}

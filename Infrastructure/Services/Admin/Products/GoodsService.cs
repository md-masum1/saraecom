﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application._Common.Models;
using Application._Common.Paging;
using Application.Admin.Products;
using Domain.Entities.Admin.Product;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Files;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Admin.Products
{
    public class GoodsService : IGoodsService
    {
        private readonly ApplicationDbContext _context;
        private readonly FileProcessor _fileProcessor;

        public GoodsService(ApplicationDbContext context, FileProcessor fileProcessor)
        {
            _context = context;
            _fileProcessor = fileProcessor;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

  
        
        public async Task<IList<ProductImage>> GetProductImage(int productId)
        {
            var productImage = await _context.ProductImages.Where(i => i.ProductId == productId).ToListAsync();

            return productImage;
        }

        public async Task<PagedList<Product>> GetProduct(ProductParams productParams)
        {
            var products = _context.Products.OrderByDescending(c => c.WhProductId).AsQueryable();

            //products = productParams.SavedProduct ? products.Where(p => p.IsActive) : products.Where(p => p.IsActive == false);

            if (!string.IsNullOrWhiteSpace(productParams.Style))
            {
                products = products.Where(p => p.ProductStyle.ToLower().Contains(productParams.Style.Trim().ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(productParams.CategoryName))
            {
                //products = products.Where(p => p.CategoryName == productParams.CategoryName);
            }

            if (!string.IsNullOrWhiteSpace(productParams.SubCategoryName))
            {
                //products = products.Where(p => p.SubCategoryName == productParams.SubCategoryName);
            }

            return await PagedList<Product>.CreateAsync(products, productParams.PageNumber, productParams.PageSize);
        }

        public async Task<Product> GetProductByStyle(string style)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.ProductStyle == style);

            return product;
        }

        public async Task<(Result result, int productId)> SaveWarehouseProduct(Product products)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.WhProductId == products.WhProductId);

            if (product != null)
            {
                product.Name = products.Name;
                product.ProductStyle = products.ProductStyle;
                product.ProductItem = products.ProductItem;

                product.New = products.New;
                product.Sale = products.Sale;
                product.Price = products.Price;
                product.SalePrice = products.SalePrice;
                product.Discount = products.Discount;
                product.Stock = products.Stock;

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return (Result.Success(), (int)product.WhProductId);

                return (Result.Failure(new List<string> { "Can't update product" }), 0);
            }
            else
            {
                products.New = false;
                products.Sale = false;
                products.Price = 0;
                products.SalePrice = 0;
                products.Discount = 0;
                products.Stock = 0;

                var productToSave = await _context.Products.AddAsync(products);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return (Result.Success(), (int)productToSave.Entity.WhProductId);

                return (Result.Failure(new List<string> { "Can't save product" }), 0);
            }
        }

        public async Task<Result> UpdateProduct(Product product)
        {
            var productToUpdate = await _context.Products.Include(p => p.ProductItem)
                .FirstOrDefaultAsync(p => p.Id == product.Id);

            if (productToUpdate != null)
            {
                productToUpdate.LongDescription = product.LongDescription.Trim();
                productToUpdate.ShortDescription = product.ShortDescription.Trim();

                productToUpdate.IsActive = product.IsActive;

                productToUpdate.New = product.New;
                productToUpdate.Sale = product.Sale;
                productToUpdate.Price = product.Price;
                productToUpdate.SalePrice = product.SalePrice;
                productToUpdate.Discount = product.Discount;
                productToUpdate.Stock = product.Stock;

                foreach (var productItem in productToUpdate.ProductItem)
                {
                    productItem.IsActive = product.IsActive;
                }

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }

            return Result.Failure(new List<string> { "Failed to update product" });
        }

        public async Task<Result> UpdateProductItem(ProductItem productItem)
        {
            var productItemToUpdate = await _context.ProductItems.FirstOrDefaultAsync(p =>
                p.Id == productItem.Id && p.ProductId == productItem.ProductId);

            if (productItemToUpdate != null)
            {
                productItemToUpdate.IsActive = productItem.IsActive;
                productItemToUpdate.ColorName = productItem.ColorName.Trim();
                productItemToUpdate.ColorCode = productItem.ColorCode.Trim();
                productItemToUpdate.SizeName = productItem.SizeName.Trim();
                productItemToUpdate.FabricName = productItem.FabricName.Trim();
                productItemToUpdate.Stock = productItem.Stock;

                if (productItem.ImageFile != null && productItem.ImageFile.Length > 0)
                {
                    _fileProcessor.DeleteFile(productItem.Images);
                    var image = await _fileProcessor.SaveImage(productItem.ImageFile, "\\Files\\ProductItemImage\\");
                    productItemToUpdate.Images = image.FilePath;
                }

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }

            return Result.Failure(new List<string> { "Failed to update product item" });
        }

        public async Task<Result> UploadProductImage(ProductImage productImage)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.Id == productImage.ProductId);

            if (product != null)
            {
                if (productImage.ImageFile != null)
                {
                    var image = await _fileProcessor.SaveImage(productImage.ImageFile, "\\Files\\ProductImage\\");
                    productImage.Image = image.FilePath;

                    await _context.AddAsync(productImage);

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();
                }
                return Result.Failure(new List<string> { "Failed to upload image" });
            }
            return Result.Failure(new List<string> { "product not found" });
        }

        public async Task<Result> DeleteProductImage(int productId, int imageId)
        {
            var itemImage = await _context.ProductImages.FirstOrDefaultAsync(c => c.Id == imageId && c.ProductId == productId);

            if (itemImage != null)
            {
                _fileProcessor.DeleteFile(itemImage.Image);
                _context.Remove(itemImage);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }

            return Result.Failure(new List<string> { "no image found" });
        }



        public async Task<Result> CreateProduct(Product product)
        {
            if (product.Id > 0)
            {
                var productToUpdate = await _context.Products.FirstOrDefaultAsync(p => p.Id == product.Id);

                if (productToUpdate != null)
                {
                    productToUpdate.Id = product.Id;
                    productToUpdate.WhProductId = product.WhProductId;
                    productToUpdate.Name = product.Name.Trim();
                    productToUpdate.ProductStyle = product.ProductStyle.Trim();
                    productToUpdate.IsActive = product.IsActive;
                    productToUpdate.ShortDescription = product.ShortDescription.Trim();
                    productToUpdate.LongDescription = product.LongDescription.Trim();
                    productToUpdate.Fabric = product.Fabric.Trim();
                    productToUpdate.CategoryId = product.CategoryId;
                    productToUpdate.SubCategoryId = product.SubCategoryId;
                    productToUpdate.New = product.New;
                    productToUpdate.Sale = product.Sale;
                    productToUpdate.Price = product.Price;
                    productToUpdate.SalePrice = product.SalePrice;
                    productToUpdate.Discount = product.Discount;
                    productToUpdate.Stock = product.Stock;

                    var result = await _context.SaveChangesAsync();

                    if (result > 0)
                        return Result.Success();

                    return Result.Failure(new List<string> { "Can't update product" });
                }
                return Result.Failure(new List<string> { "product not found" });
            }
            else
            {
                var productToCreate =
                    await _context.Products.FirstOrDefaultAsync(p => p.ProductStyle == product.ProductStyle);

                if (productToCreate != null)
                    return Result.Failure(new List<string> { "Style already exist" });

                await _context.AddAsync(product);

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "Can't update product" });
            }
        }

        public async Task<IList<ProductColor>> GetProductColor(int productId)
        {
            var productColor = await _context.ProductColors.Where(pc => pc.ProductId == productId).ToListAsync();

            return productColor;
        }
        public async Task<Result> AddProductColor(ProductColor productColor)
        {
            if (productColor.Id > 0)
            {
                var productColorToUpdate = await _context.ProductColors.FirstOrDefaultAsync(c => c.Id == productColor.Id);

                if (productColorToUpdate == null)
                    return Result.Failure(new List<string> { "Invalid entry, Please try again" });

                if (productColorToUpdate.ProductId == productColor.ProductId && productColorToUpdate.ColorId == productColor.ColorId)
                    return Result.Failure(new List<string> { "color already exist" });

                productColorToUpdate.ProductId = productColor.ProductId;
                productColorToUpdate.ColorId = productColor.ColorId;

                if (productColor.ImageFile != null && productColor.ImageFile.Length > 0)
                {
                    _fileProcessor.DeleteFile(productColorToUpdate.Images);
                    var image = await _fileProcessor.SaveImage(productColor.ImageFile, "\\Files\\ProductColorImage\\");
                    productColorToUpdate.Images = image.FilePath;
                }

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "can't update product color" });
            }
            else
            {
                var image = await _fileProcessor.SaveImage(productColor.ImageFile, "\\Files\\ProductColorImage\\");
                productColor.Images = image.FilePath;

                await _context.AddAsync(productColor);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "can't save product color" });
            }
        }
        public async Task<Result> DeleteProductColor(int colorId, int productId)
        {
            var productColor =
                await _context.ProductColors.FirstOrDefaultAsync(cp =>
                    cp.ColorId == colorId && cp.ProductId == productId);

            if (productColor != null)
            {
                _fileProcessor.DeleteFile(productColor.Images);
                _context.Remove(productColor);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }

            return Result.Failure(new List<string> { "product color not found" });
        }

        public async Task<IList<ProductSize>> GetProductSize(int productId)
        {
            var productColor = await _context.ProductSizes.Where(pc => pc.ProductId == productId).ToListAsync();

            return productColor;
        }
        public async Task<Result> AddProductSize(ProductSize productSize)
        {
            var getProductSize = await _context.ProductSizes.FirstOrDefaultAsync(ps =>
                ps.ProductId == productSize.ProductId && ps.SizeId == productSize.SizeId &&
                ps.ColorId == productSize.ColorId);

            if(getProductSize != null)
                return Result.Failure(new List<string> { "size already exist" });

            if (productSize.Id > 0)
            {
                var productSizeToUpdate = await _context.ProductSizes.FirstOrDefaultAsync(ps => ps.Id == productSize.Id && ps.ProductId == productSize.ProductId);

                if (productSizeToUpdate == null)
                    return Result.Failure(new List<string> { "Invalid entry, Please try again" });

                productSizeToUpdate.SizeId = productSize.SizeId;
                productSizeToUpdate.ColorId = productSize.ColorId;
                productSizeToUpdate.Price = productSize.Price;
                productSizeToUpdate.Stock = productSize.Stock;

                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "can't update product size" });
            }
            else
            {
                await _context.AddAsync(productSize);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();

                return Result.Failure(new List<string> { "can't save product size" });
            }
        }
        public async Task<Result> DeleteProductSize(int id)
        {
            var productSize = await _context.ProductSizes.FirstOrDefaultAsync(ps => ps.Id == id);

            if (productSize != null)
            {
                _context.Remove(productSize);
                var result = await _context.SaveChangesAsync();

                if (result > 0)
                    return Result.Success();
            }

            return Result.Failure(new List<string> { "product size not found" });
        }
    }
}

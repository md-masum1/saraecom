﻿using System;
using System.Collections.Generic;
using System.Text;
using Application._Common.Interfaces;

namespace Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}

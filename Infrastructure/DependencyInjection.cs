﻿using System.Text;
using Application._Common.Interfaces;
using Application.Admin.Admin;
using Application.Admin.Products;
using Application.Admin.Setup.Categories;
using Application.Admin.Setup.Colors;
using Application.Admin.Setup.HomePageBanners;
using Application.Admin.Setup.HomePageSliders;
using Application.Admin.Setup.Menus;
using Application.Admin.Setup.PaymentTypes;
using Application.Admin.Setup.Sizes;
using Application.Admin.Setup.Tags;
using Application.Ecom.EcomProduct;
using Application.Ecom.Home;
using Infrastructure.Identity;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Files;
using Infrastructure.Services;
using Infrastructure.Services.Admin.Admin;
using Infrastructure.Services.Admin.Products;
using Infrastructure.Services.Admin.Setup;
using Infrastructure.Services.Ecom;
using Infrastructure.Services.PosService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(x => {
                x.UseLazyLoadingProxies();
                x.UseOracle(configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName));
            });

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

            IdentityBuilder builder = services.AddIdentityCore<User>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 4;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireLowercase = false;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationDbContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                            .GetBytes(configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                    };
                });

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();

                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddScoped<AuthorizationFilter>();
            services.AddTransient<IDateTime, DateTimeService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<ILogToDatabaseService, LogToDatabaseService>();
            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<IPosService, PosService>();
            services.AddTransient<IGoodsService, GoodsService>();
            services.AddTransient<IMenuService, MenuService>();
            services.AddTransient<IPaymentTypeService, PaymentTypeService>();
            services.AddScoped<FileProcessor>();
            services.AddTransient<ISliderService, SliderService>();
            services.AddTransient<IBannerService, BannerService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IColorService, ColorService>();
            services.AddTransient<ISizeService, SizeService>();
            services.AddTransient<ITagService, TagService>();

            //For Ecom
            services.AddTransient<IHomeService, HomeService>();
            services.AddTransient<IEcomProductService, EcomProductService>();

            return services;
        }
    }
}

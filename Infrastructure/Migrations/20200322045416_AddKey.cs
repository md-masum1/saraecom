﻿using Microsoft.EntityFrameworkCore.Migrations;
using Oracle.EntityFrameworkCore.Metadata;

namespace Infrastructure.Migrations
{
    public partial class AddKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ProductTags",
                nullable: false,
                defaultValue: 0)
                .Annotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ProductSizes",
                nullable: false,
                defaultValue: 0)
                .Annotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ProductColors",
                nullable: false,
                defaultValue: 0)
                .Annotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_ProductTags_Id",
                table: "ProductTags",
                column: "Id");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_ProductSizes_Id",
                table: "ProductSizes",
                column: "Id");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_ProductColors_Id",
                table: "ProductColors",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_ProductTags_Id",
                table: "ProductTags");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_ProductSizes_Id",
                table: "ProductSizes");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_ProductColors_Id",
                table: "ProductColors");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProductTags");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProductSizes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProductColors");
        }
    }
}

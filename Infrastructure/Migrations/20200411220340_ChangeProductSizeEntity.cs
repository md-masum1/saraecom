﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class ChangeProductSizeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ColorId",
                table: "ProductSizes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductSizes_ColorId",
                table: "ProductSizes",
                column: "ColorId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductSizes_Colors_ColorId",
                table: "ProductSizes",
                column: "ColorId",
                principalTable: "Colors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductSizes_Colors_ColorId",
                table: "ProductSizes");

            migrationBuilder.DropIndex(
                name: "IX_ProductSizes_ColorId",
                table: "ProductSizes");

            migrationBuilder.DropColumn(
                name: "ColorId",
                table: "ProductSizes");
        }
    }
}

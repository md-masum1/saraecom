﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Application._Common.Exceptions;
using Application._Common.Interfaces;
using Application._Common.Models;
using Application.Auth;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public IdentityService(ApplicationDbContext context,
            IConfiguration config,
            UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _context = context;
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<object> Login(UserForLoginDto userForLogin)
        {
            if (!string.IsNullOrWhiteSpace(userForLogin.UserName) && !string.IsNullOrWhiteSpace(userForLogin.Password))
            {
                var user = await _context.Users.FirstOrDefaultAsync(c => c.EmployeeId == userForLogin.UserName) ?? 
                           (await _userManager.FindByEmailAsync(userForLogin.UserName) ?? await _userManager.FindByNameAsync(userForLogin.UserName));

                if (user == null)
                {
                    throw new UnauthorizedAccessException("User not found! Please register");
                }

                var result = await _signInManager.CheckPasswordSignInAsync(user, userForLogin.Password, false);

                if (result.Succeeded)
                {
                    UserForReturnDto appUser = new UserForReturnDto
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        Email = user.Email,
                        EmployeeId = user.EmployeeId,
                        PhoneNumber = user.PhoneNumber

                    };

                    return new
                    {
                        token = GenerateJwtToken(user).Result,
                        user = appUser
                    };
                }

                throw new UnauthorizedAccessException("Invalid username or password");
            }


            throw new NotFoundException(nameof(User), userForLogin.UserName);
        }

        public async Task<(Result Result, int UserId)> Register(UserForRegisterDto userForRegister)
        {
            var checkUser = await _context.Users.FirstOrDefaultAsync(c => c.UserName == userForRegister.UserName);
            if (checkUser != null)
                return (Result.Failure(new List<string> { "User Already Exist" }), checkUser.Id);

            if (!string.IsNullOrWhiteSpace(userForRegister.EmployeeId))
            {
                checkUser = await _context.Users.FirstOrDefaultAsync(c => c.EmployeeId == userForRegister.EmployeeId);
                if (checkUser != null)
                    return (Result.Failure(new List<string> { "User Already Exist" }), checkUser.Id);
            }

            var user = new User
            {
                UserName = userForRegister.UserName,
                Email = userForRegister.Email,
                EmployeeId = userForRegister.EmployeeId,
                PhoneNumber = userForRegister.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, userForRegister.Password);

            if (result.Succeeded)
            {
                var userForRole = await _userManager.FindByNameAsync(user.UserName);

                if (!string.IsNullOrWhiteSpace(userForRole.EmployeeId))
                {
                    await _userManager.AddToRolesAsync(userForRole, new[] { UsersRole.Employee.ToString() });
                }
                else
                {
                    await _userManager.AddToRolesAsync(userForRole, new[] { UsersRole.Customer.ToString() });
                }

                
            }

            return (result.ToApplicationResult(), user.Id);
        }

        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            if (!string.IsNullOrWhiteSpace(user.EmployeeId))
            {
                claims.Add(new Claim(ClaimTypes.SerialNumber, user.EmployeeId));
            }

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}

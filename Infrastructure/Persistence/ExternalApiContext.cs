﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public static class ExternalApiContext<TEntity> where TEntity : class
    {
        public static async Task<TEntity> GetAsync(string url, string action)
        {
            using var client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(url);
                using var response = await client.GetAsync(action);
                if (response.IsSuccessStatusCode)
                {
                    using var content = response.Content;
                    var result = await content.ReadAsAsync<TEntity>();
                    return result;
                }

                return null;
            }
            catch (HttpRequestException e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}

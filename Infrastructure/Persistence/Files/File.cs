﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.Files
{
    public class File
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileUrl { get; set; }
    }
}

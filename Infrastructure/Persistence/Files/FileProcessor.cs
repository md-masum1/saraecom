﻿using System;
using System.IO;
using System.Threading.Tasks;
using Application._Common.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Persistence.Files
{
    public class FileProcessor
    {
        private readonly IWebHostEnvironment _environment;

        public FileProcessor(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        public async Task<File> SaveImage(IFormFile file, string path)
        {
            if (file.Length > 0)
            {
                string fileName = Guid.NewGuid().ToString();
                var imageExtension = Path.GetExtension(file.FileName);
                imageExtension = imageExtension.ToLower();
                fileName += imageExtension;

                if (imageExtension == ".jpg" || imageExtension == ".jpeg" || imageExtension == ".png")
                {
                    if (!Directory.Exists(_environment.WebRootPath + path))
                        Directory.CreateDirectory(_environment.WebRootPath + path);

                    await using FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + path + fileName);
                    await file.CopyToAsync(fileStream);
                    await fileStream.FlushAsync();
                    return new File
                    {
                        FileName = fileName,
                        FilePath = path + fileName,
                        FileUrl = _environment.WebRootPath + path + fileName
                    };
                }
                throw new Exception("un-supported image type");
            }
            throw new NotFoundException("No image found", file);
        }

        public bool DeleteFile(string path)
        {
            if (!System.IO.File.Exists(_environment.WebRootPath + path)) return false;
            System.IO.File.Delete(_environment.WebRootPath + path);
            return true;
        }
    }
}

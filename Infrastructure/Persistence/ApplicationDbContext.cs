﻿using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using Domain._Common;
using Domain.Entities.Admin.Product;
using Domain.Entities.Admin.Setup;
using Domain.Entities.Admin.Setup.Menus;
using Domain.Entities.Logger;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>,
        UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options,
            ICurrentUserService currentUserService,
            IDateTime dateTime) : base(options)
        {
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public DbSet<RequestLoggerEntity> LoggerEntities { get; set; }

        //Setup
        public DbSet<Menu> Menus { get; set; }
        public DbSet<SubMenu> SubMenus { get; set; }
        public DbSet<SubSubMenu> SubSubMenus { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<HomePageSlider> HomePageSliders { get; set; }
        public DbSet<HomePageBanner> HomePageBanners { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Size> Sizes { get; set; }

        //Product
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductItem> ProductItems { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<ProductTag> ProductTags { get; set; }
        public DbSet<ProductSize> ProductSizes { get; set; }
        public DbSet<ProductColor> ProductColors { get; set; }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.EmployeeId;
                        entry.Entity.CreateDate = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdateBy = _currentUserService.EmployeeId;
                        entry.Entity.UpdateDate = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);

            // start identity
            builder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
            // end identity

            // start product
            builder.Entity<ProductTag>(productTag =>
            {
                productTag.HasKey(pt => new { pt.ProductId, pt.TagId });

                productTag.HasOne(pt => pt.Tag)
                    .WithMany(t => t.ProductTags)
                    .HasForeignKey(pt => pt.TagId)
                    .IsRequired();

                productTag.HasOne(pt => pt.Product)
                    .WithMany(t => t.ProductTags)
                    .HasForeignKey(pt => pt.ProductId)
                    .IsRequired();
            });

            builder.Entity<ProductColor>(productColor =>
            {
                productColor.HasKey(pc => new {pc.ProductId, pc.ColorId});

                productColor.HasOne(pc => pc.Color)
                    .WithMany(c => c.ProductColors)
                    .HasForeignKey(pc => pc.ColorId)
                    .IsRequired();

                productColor.HasOne(pc => pc.Product)
                    .WithMany(c => c.ProductColors)
                    .HasForeignKey(pc => pc.ProductId)
                    .IsRequired();
            });

            //builder.Entity<ProductSize>(productSize =>
            //{
            //    productSize.HasKey(pc => new { pc.ProductId, pc.SizeId });

            //    productSize.HasOne(pc => pc.Size)
            //        .WithMany(c => c.ProductSizes)
            //        .HasForeignKey(pc => pc.SizeId)
            //        .IsRequired();

            //    productSize.HasOne(pc => pc.Product)
            //        .WithMany(c => c.ProductSizes)
            //        .HasForeignKey(pc => pc.ProductId)
            //        .IsRequired();
            //});

            builder.Entity<Product>(entity => {
                entity.HasIndex(e => e.ProductStyle).IsUnique();
            });

            builder.Entity<Product>(entity => {
                entity.HasIndex(e => e.WhProductId).IsUnique();
            });
            // end product

            // start setup
            builder.Entity<Color>(entity => {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<Tag>(entity => {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<Size>(entity => {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<Category>(entity => {
                entity.HasIndex(e => e.Name).IsUnique();
            });

            builder.Entity<SubCategory>(entity => {
                entity.HasIndex(e => e.Name).IsUnique();
            });
            // end setup


        }
    }
}

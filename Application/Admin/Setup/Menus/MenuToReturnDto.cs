﻿using System.Collections.Generic;
using Application._Common.Mappings;
using Domain.Entities.Admin.Setup.Menus;

namespace Application.Admin.Setup.Menus
{
    public sealed class MenuToReturnDto : IMapFrom<Menu>
    {
        public MenuToReturnDto()
        {
            SubMenus = new List<SubMenuToReturnDto>();
        }
        public int Id { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public ICollection<SubMenuToReturnDto> SubMenus { get; set; }
    }

    public sealed class SubMenuToReturnDto : IMapFrom<SubMenu>
    {
        public SubMenuToReturnDto()
        {
            SubSubMenus = new List<SubSubMenuToReturnDto>();
        }
        public int Id { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public int MenuId { get; set; }
        public ICollection<SubSubMenuToReturnDto> SubSubMenus { get; set; }
    }

    public class SubSubMenuToReturnDto : IMapFrom<SubSubMenu>
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public int SubMenuId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup.Menus;

namespace Application.Admin.Setup.Menus
{
    public interface IMenuService : IDisposable
    {
        Task<List<Menu>> GetAllMenus();
        Task<List<SubMenu>> GetAllSubMenus(int menuId);
        Task<List<SubSubMenu>> GetAllSubSubMenus(int subMenuId);

        Task<Result> SaveMenu(Menu menu);
        Task<Result> SaveSubMenu(SubMenu subMenu);
        Task<Result> SaveSubSubMenu(SubSubMenu subSubMenu);

        Task<Result> DeleteMenu(int menuId);
        Task<Result> DeleteSubMenu(int menuId, int subMenuId);
        Task<Result> DeleteSubSubMenu(int subMenuId, int subSubMenuId);
    }
}

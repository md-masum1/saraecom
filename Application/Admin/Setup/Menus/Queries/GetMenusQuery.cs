﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Menus.Queries
{
    public class GetMenusQuery : IRequest<IEnumerable<MenuToReturnDto>>
    {
        public class GetMenusQueryHandler : IRequestHandler<GetMenusQuery, IEnumerable<MenuToReturnDto>>
        {
            private readonly IMenuService _menuService;
            private readonly IMapper _mapper;

            public GetMenusQueryHandler(IMenuService menuService, IMapper mapper)
            {
                _menuService = menuService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<MenuToReturnDto>> Handle(GetMenusQuery request, CancellationToken cancellationToken)
            {
                var result = await _menuService.GetAllMenus();

                return _mapper.Map<IEnumerable<MenuToReturnDto>>(result);
            }
        }
    }
}

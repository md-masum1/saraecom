﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Menus.Queries
{
    public class GetSubSubMenusQuery : IRequest<IEnumerable<SubSubMenuToReturnDto>>
    {
        public int SubMenuId { get; set; }

        public class GetSubSubMenusQueryHandler : IRequestHandler<GetSubSubMenusQuery, IEnumerable<SubSubMenuToReturnDto>>
        {
            private readonly IMenuService _menuService;
            private readonly IMapper _mapper;

            public GetSubSubMenusQueryHandler(IMenuService menuService, IMapper mapper)
            {
                _menuService = menuService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<SubSubMenuToReturnDto>> Handle(GetSubSubMenusQuery request, CancellationToken cancellationToken)
            {
                var result = await _menuService.GetAllSubSubMenus(request.SubMenuId);
                return _mapper.Map<IEnumerable<SubSubMenuToReturnDto>>(result);
            }
        }
    }
}

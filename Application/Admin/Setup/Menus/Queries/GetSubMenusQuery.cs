﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Menus.Queries
{
    public class GetSubMenusQuery : IRequest<IEnumerable<SubMenuToReturnDto>>
    {
        public int MenuId { get; set; }

        public class GetSubMenusQueryHandler : IRequestHandler<GetSubMenusQuery, IEnumerable<SubMenuToReturnDto>>
        {
            private readonly IMenuService _menuService;
            private readonly IMapper _mapper;

            public GetSubMenusQueryHandler(IMenuService menuService, IMapper mapper)
            {
                _menuService = menuService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<SubMenuToReturnDto>> Handle(GetSubMenusQuery request, CancellationToken cancellationToken)
            {
                var result = await _menuService.GetAllSubMenus(request.MenuId);

                return _mapper.Map<IEnumerable<SubMenuToReturnDto>>(result);
            }
        }
    }
}

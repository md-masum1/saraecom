﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Menus.Commands.DeleteCommand
{
    public class DeleteSubSubMenuCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int SubMenuId { get; set; }

        public class DeleteSubSubMenuCommandHandler : IRequestHandler<DeleteSubSubMenuCommand, Result>
        {
            private readonly IMenuService _menuService;

            public DeleteSubSubMenuCommandHandler(IMenuService menuService)
            {
                _menuService = menuService;
            }
            public async Task<Result> Handle(DeleteSubSubMenuCommand request, CancellationToken cancellationToken)
            {
                return await _menuService.DeleteSubSubMenu(request.SubMenuId, request.Id);
            }
        }
    }
}

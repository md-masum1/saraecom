﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Menus.Commands.DeleteCommand
{
    public class DeleteMenuCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteMenuCommandHandler : IRequestHandler<DeleteMenuCommand, Result>
        {
            private readonly IMenuService _menuService;

            public DeleteMenuCommandHandler(IMenuService menuService)
            {
                _menuService = menuService;
            }
            public async Task<Result> Handle(DeleteMenuCommand request, CancellationToken cancellationToken)
            {
                return await _menuService.DeleteMenu(request.Id);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Menus.Commands.DeleteCommand
{
    public class DeleteSubMenuCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int MenuId { get; set; }

        public class DeleteSubMenuCommandHandler : IRequestHandler<DeleteSubMenuCommand, Result>
        {
            private readonly IMenuService _menuService;

            public DeleteSubMenuCommandHandler(IMenuService menuService)
            {
                _menuService = menuService;
            }
            public async Task<Result> Handle(DeleteSubMenuCommand request, CancellationToken cancellationToken)
            {
                return await _menuService.DeleteSubMenu(request.MenuId, request.Id);
            }
        }
    }
}

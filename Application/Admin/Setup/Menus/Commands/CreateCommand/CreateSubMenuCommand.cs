﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using AutoMapper;
using Domain.Entities.Admin.Setup.Menus;
using MediatR;

namespace Application.Admin.Setup.Menus.Commands.CreateCommand
{
    public class CreateSubMenuCommand : IRequest<Result>
    {
        public int Id { get; set; } = 0;
        [Required]
        public byte DisplayOrder { get; set; } = 0;
        public string Path { get; set; }
        [Required]
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        [Required]
        public int MenuId { get; set; }

        public class CreateSubMenuCommandHandler : IRequestHandler<CreateSubMenuCommand, Result>
        {
            private readonly IMenuService _menuService;
            private readonly IMapper _mapper;

            public CreateSubMenuCommandHandler(IMenuService menuService, IMapper mapper)
            {
                _menuService = menuService;
                _mapper = mapper;
            }
            public async Task<Result> Handle(CreateSubMenuCommand request, CancellationToken cancellationToken)
            {
                var subMenu = new SubMenu
                {
                    Id = request.Id,
                    Title = request.Title,
                    DisplayOrder = request.DisplayOrder,
                    Path = request.Path,
                    Type = request.Type,
                    MegaMenu = request.MegaMenu,
                    MegaMenuType = request.MegaMenuType,
                    MenuId = request.MenuId
                };

                return await _menuService.SaveSubMenu(subMenu);
            }
        }
    }
}

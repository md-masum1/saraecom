﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using AutoMapper;
using Domain.Entities.Admin.Setup.Menus;
using MediatR;

namespace Application.Admin.Setup.Menus.Commands.CreateCommand
{
    public class CreateMenuCommand : IRequest<Result>
    {
        public int Id { get; set; } = 0;
        [Required]
        public byte DisplayOrder { get; set; } = 0;
        public string Path { get; set; }
        [Required]
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }

        public class CreateMenuCommandHandler : IRequestHandler<CreateMenuCommand, Result>
        {
            private readonly IMenuService _menuService;
            private readonly IMapper _mapper;

            public CreateMenuCommandHandler(IMenuService menuService, IMapper mapper)
            {
                _menuService = menuService;
                _mapper = mapper;
            }
            public async Task<Result> Handle(CreateMenuCommand request, CancellationToken cancellationToken)
            {
                var menu = new Menu
                {
                    Id = request.Id,
                    Title = request.Title,
                    DisplayOrder = request.DisplayOrder,
                    Path = request.Path,
                    Type = request.Type,
                    MegaMenu = request.MegaMenu,
                    MegaMenuType = request.MegaMenuType
                };

                return await _menuService.SaveMenu(menu);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Sizes.Commands
{
    public class DeleteSizeCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteSizeCommandHandler : IRequestHandler<DeleteSizeCommand, Result>
        {
            private readonly ISizeService _sizeService;

            public DeleteSizeCommandHandler(ISizeService sizeService)
            {
                _sizeService = sizeService;
            }
            public async Task<Result> Handle(DeleteSizeCommand request, CancellationToken cancellationToken)
            {
                return await _sizeService.DeleteSize(request.Id);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;

namespace Application.Admin.Setup.Sizes.Commands
{
    public class CreateSizeCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public class CreateSizeCommandHandler : IRequestHandler<CreateSizeCommand, Result>
        {
            private readonly ISizeService _sizeService;

            public CreateSizeCommandHandler(ISizeService sizeService)
            {
                _sizeService = sizeService;
            }
            public async Task<Result> Handle(CreateSizeCommand request, CancellationToken cancellationToken)
            {
                var size = new Size
                {
                    Id = request.Id,
                    Name = request.Name.Trim()
                };

                return await _sizeService.CreateSize(size);
            }
        }
    }
}

﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Sizes
{
    public class SizeToReturnDto : IMapFrom<Size>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

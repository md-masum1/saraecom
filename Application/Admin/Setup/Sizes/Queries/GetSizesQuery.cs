﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Sizes.Queries
{
    public class GetSizesQuery : IRequest<IList<SizeToReturnDto>>
    {
        public class GetSizesQueryHandler : IRequestHandler<GetSizesQuery, IList<SizeToReturnDto>>
        {
            private readonly ISizeService _sizeService;
            private readonly IMapper _mapper;

            public GetSizesQueryHandler(ISizeService sizeService, IMapper mapper)
            {
                _sizeService = sizeService;
                _mapper = mapper;
            }
            public async Task<IList<SizeToReturnDto>> Handle(GetSizesQuery request, CancellationToken cancellationToken)
            {
                var sizes = await _sizeService.GetSizes();

                return _mapper.Map<IList<SizeToReturnDto>>(sizes);
            }
        }
    }
}

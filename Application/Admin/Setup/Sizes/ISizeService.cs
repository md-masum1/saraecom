﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Sizes
{
    public interface ISizeService : IDisposable
    {
        Task<IList<Size>> GetSizes();

        Task<Result> CreateSize(Size size);

        Task<Result> DeleteSize(int id);
    }
}

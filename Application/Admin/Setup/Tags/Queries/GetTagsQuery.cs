﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Tags.Queries
{
    public class GetTagsQuery : IRequest<IList<TagToReturnDto>>
    {
        public class GetTagsQueryHandler : IRequestHandler<GetTagsQuery, IList<TagToReturnDto>>
        {
            private readonly ITagService _tagService;
            private readonly IMapper _mapper;

            public GetTagsQueryHandler(ITagService tagService, IMapper mapper)
            {
                _tagService = tagService;
                _mapper = mapper;
            }
            public async Task<IList<TagToReturnDto>> Handle(GetTagsQuery request, CancellationToken cancellationToken)
            {
                var tags = await _tagService.GetTags();

                return _mapper.Map<IList<TagToReturnDto>>(tags);
            }
        }
    }
}

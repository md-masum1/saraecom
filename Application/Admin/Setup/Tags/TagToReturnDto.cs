﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Tags
{
    public class TagToReturnDto: IMapFrom<Tag>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

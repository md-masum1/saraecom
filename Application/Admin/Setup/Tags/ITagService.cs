﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Tags
{
    public interface ITagService : IDisposable
    {
        Task<IList<Tag>> GetTags();

        Task<Result> CreateTag(Tag tag);

        Task<Result> DeleteTag(int id);
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;

namespace Application.Admin.Setup.Tags.Commands
{
    public class CreateTagCommand : IRequest<Result>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public class CreateTagCommandHandler : IRequestHandler<CreateTagCommand, Result>
        {
            private readonly ITagService _tagService;

            public CreateTagCommandHandler(ITagService tagService)
            {
                _tagService = tagService;
            }
            public async Task<Result> Handle(CreateTagCommand request, CancellationToken cancellationToken)
            {
                var tag = new Tag
                {
                    Id = request.Id,
                    Name = request.Name
                };

                return await _tagService.CreateTag(tag);
            }
        }
    }
}

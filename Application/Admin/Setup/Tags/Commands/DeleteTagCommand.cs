﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Tags.Commands
{
    public class DeleteTagCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public class DeleteTagCommandHandler : IRequestHandler<DeleteTagCommand, Result>
        {
            private readonly ITagService _tagService;

            public DeleteTagCommandHandler(ITagService tagService)
            {
                _tagService = tagService;
            }
            public async Task<Result> Handle(DeleteTagCommand request, CancellationToken cancellationToken)
            {
                return await _tagService.DeleteTag(request.Id);
            }
        }
    }
}

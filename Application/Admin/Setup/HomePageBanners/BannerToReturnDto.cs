﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.HomePageBanners
{
    public class BannerToReturnDto : IMapFrom<HomePageBanner>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavigateUrl { get; set; }
        public byte DisplayOrder { get; set; }
        public string ImagePath { get; set; }
    }
}

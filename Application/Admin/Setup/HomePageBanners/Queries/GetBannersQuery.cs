﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.HomePageBanners.Queries
{
    public class GetBannersQuery : IRequest<IList<BannerToReturnDto>>
    {
        public class GetBannersQueryHandler : IRequestHandler<GetBannersQuery, IList<BannerToReturnDto>>
        {
            private readonly IBannerService _bannerService;
            private readonly IMapper _mapper;

            public GetBannersQueryHandler(IBannerService bannerService, IMapper mapper)
            {
                _bannerService = bannerService;
                _mapper = mapper;
            }
            public async Task<IList<BannerToReturnDto>> Handle(GetBannersQuery request, CancellationToken cancellationToken)
            {
                var banners = await _bannerService.GetBanners();
                return _mapper.Map<IList<BannerToReturnDto>>(banners);
            }
        }
    }
}

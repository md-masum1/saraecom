﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.HomePageBanners
{
    public interface IBannerService : IDisposable
    {
        Task<IList<HomePageBanner>> GetBanners();
        Task<HomePageBanner> GetBanner(int id);
        Task<Result> CreateBanner(HomePageBanner homePageBanner);
        Task<Result> DeleteBanner(int id);
    }
}

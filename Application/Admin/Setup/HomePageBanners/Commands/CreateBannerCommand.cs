﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Setup.HomePageBanners.Commands
{
    public class CreateBannerCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavigateUrl { get; set; }
        public byte DisplayOrder { get; set; }
        public IFormFile ImageFile { get; set; }

        public class CreateBannerCommandHandler : IRequestHandler<CreateBannerCommand, Result>
        {
            private readonly IBannerService _bannerService;

            public CreateBannerCommandHandler(IBannerService bannerService)
            {
                _bannerService = bannerService;
            }
            public async Task<Result> Handle(CreateBannerCommand request, CancellationToken cancellationToken)
            {
                var banner = new HomePageBanner
                {
                    Id = request.Id,
                    Title = request.Title,
                    Description = request.Description,
                    NavigateUrl = request.NavigateUrl,
                    DisplayOrder = request.DisplayOrder,
                    ImageFile = request.ImageFile
                };

                return await _bannerService.CreateBanner(banner);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.HomePageBanners.Commands
{
    public class DeleteBannerCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteBannerCommandHandler : IRequestHandler<DeleteBannerCommand, Result>
        {
            private readonly IBannerService _bannerService;

            public DeleteBannerCommandHandler(IBannerService bannerService)
            {
                _bannerService = bannerService;
            }
            public async Task<Result> Handle(DeleteBannerCommand request, CancellationToken cancellationToken)
            {
                return await _bannerService.DeleteBanner(request.Id);
            }
        }
    }
}

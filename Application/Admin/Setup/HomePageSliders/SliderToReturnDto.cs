﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.HomePageSliders
{
    public class SliderToReturnDto : IMapFrom<HomePageSlider>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavigateUrl { get; set; }
        public byte DisplayOrder { get; set; }
        public string ImagePath { get; set; }
    }
}

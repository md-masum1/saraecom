﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.HomePageSliders.Queries
{
    public class GetSliderQuery : IRequest<SliderToReturnDto>
    {
        public int Id { get; set; }

        public class GetSliderQueryHandler : IRequestHandler<GetSliderQuery, SliderToReturnDto>
        {
            private readonly ISliderService _sliderService;
            private readonly IMapper _mapper;

            public GetSliderQueryHandler(ISliderService sliderService, IMapper mapper)
            {
                _sliderService = sliderService;
                _mapper = mapper;
            }
            public async Task<SliderToReturnDto> Handle(GetSliderQuery request, CancellationToken cancellationToken)
            {
                var slider = await _sliderService.GetSlider(request.Id);
                return _mapper.Map<SliderToReturnDto>(slider);
            }
        }
    }
}

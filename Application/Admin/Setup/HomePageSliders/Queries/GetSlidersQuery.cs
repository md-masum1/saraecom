﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.HomePageSliders.Queries
{
    public class GetSlidersQuery : IRequest<IList<SliderToReturnDto>>
    {
        public class GetSlidersQueryHandler : IRequestHandler<GetSlidersQuery, IList<SliderToReturnDto>>
        {
            private readonly ISliderService _sliderService;
            private readonly IMapper _mapper;

            public GetSlidersQueryHandler(ISliderService sliderService, IMapper mapper)
            {
                _sliderService = sliderService;
                _mapper = mapper;
            }
            public async Task<IList<SliderToReturnDto>> Handle(GetSlidersQuery request, CancellationToken cancellationToken)
            {
                var sliders = await _sliderService.GetSliders();
                return _mapper.Map<IList<SliderToReturnDto>>(sliders);
            }
        }
    }
}

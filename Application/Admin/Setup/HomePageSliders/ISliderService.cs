﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.HomePageSliders
{
    public interface ISliderService : IDisposable
    {
        Task<IList<HomePageSlider>> GetSliders();
        Task<HomePageSlider> GetSlider(int id);
        Task<Result> CreateSlider(HomePageSlider homePageSlider);
        Task<Result> DeleteSlider(int id);
    }
}

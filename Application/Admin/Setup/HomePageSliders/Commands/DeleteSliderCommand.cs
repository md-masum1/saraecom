﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.HomePageSliders.Commands
{
    public class DeleteSliderCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteSliderCommandHandler : IRequestHandler<DeleteSliderCommand, Result>
        {
            private readonly ISliderService _sliderService;

            public DeleteSliderCommandHandler(ISliderService sliderService)
            {
                _sliderService = sliderService;
            }
            public async Task<Result> Handle(DeleteSliderCommand request, CancellationToken cancellationToken)
            {
                return await _sliderService.DeleteSlider(request.Id);
            }
        }
    }
}

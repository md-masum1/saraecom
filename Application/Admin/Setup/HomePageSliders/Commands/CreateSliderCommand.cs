﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Setup.HomePageSliders.Commands
{
    public class CreateSliderCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NavigateUrl { get; set; }
        [Required]
        public byte DisplayOrder { get; set; }
        public IFormFile ImageFile { get; set; }

        public class CreateSliderCommandHandler : IRequestHandler<CreateSliderCommand, Result>
        {
            private readonly ISliderService _sliderService;

            public CreateSliderCommandHandler(ISliderService sliderService)
            {
                _sliderService = sliderService;
            }
            public async Task<Result> Handle(CreateSliderCommand request, CancellationToken cancellationToken)
            {
                var slider = new HomePageSlider
                {
                    Id = request.Id,
                    Title = request.Title,
                    Description = request.Description,
                    NavigateUrl = request.NavigateUrl,
                    DisplayOrder = request.DisplayOrder,
                    ImageFile = request.ImageFile
                };

                return await _sliderService.CreateSlider(slider);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.PaymentTypes
{
    public interface IPaymentTypeService : IDisposable
    {
        Task<Result> CreatePaymentType(PaymentType paymentType);
        Task<Result> UpdatePaymentType(PaymentType paymentType);
        Task<Result> DeletePaymentType(int id);
        Task<IList<PaymentType>> GetPaymentTypes();
        Task<PaymentType> GetPaymentType(int id);
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.PaymentTypes.Commands
{
    public class DeletePaymentTypeCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeletePaymentTypeCommandHandler : IRequestHandler<DeletePaymentTypeCommand, Result>
        {
            private readonly IPaymentTypeService _paymentTypeService;

            public DeletePaymentTypeCommandHandler(IPaymentTypeService paymentTypeService)
            {
                _paymentTypeService = paymentTypeService;
            }
            public async Task<Result> Handle(DeletePaymentTypeCommand request, CancellationToken cancellationToken)
            {
                return await _paymentTypeService.DeletePaymentType(request.Id);
            }
        }
    }
}

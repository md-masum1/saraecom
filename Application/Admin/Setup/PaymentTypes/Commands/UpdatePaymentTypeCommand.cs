﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Setup.PaymentTypes.Commands
{
    public class UpdatePaymentTypeCommand : IRequest<Result>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public IFormFile ImageFile { get; set; }
        public byte DisplayOrder { get; set; }

        public class UpdatePaymentTypeCommandHandler : IRequestHandler<UpdatePaymentTypeCommand, Result>
        {
            private readonly IPaymentTypeService _paymentTypeService;

            public UpdatePaymentTypeCommandHandler(IPaymentTypeService paymentTypeService)
            {
                _paymentTypeService = paymentTypeService;
            }
            public async Task<Result> Handle(UpdatePaymentTypeCommand request, CancellationToken cancellationToken)
            {
                var paymentType = new PaymentType
                {
                    Id = request.Id,
                    Name = request.Name,
                    ImageFile = request.ImageFile,
                    DisplayOrder = request.DisplayOrder
                };

                return await _paymentTypeService.UpdatePaymentType(paymentType);
            }
        }
    }
}

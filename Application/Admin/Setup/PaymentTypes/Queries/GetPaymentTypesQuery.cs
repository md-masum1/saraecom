﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.PaymentTypes.Queries
{
    public class GetPaymentTypesQuery : IRequest<IEnumerable<PaymentTypeToReturnDto>>
    {
        public class GetPaymentTypesQueryHandler : IRequestHandler<GetPaymentTypesQuery, IEnumerable<PaymentTypeToReturnDto>>
        {
            private readonly IPaymentTypeService _paymentTypeService;
            private readonly IMapper _mapper;

            public GetPaymentTypesQueryHandler(IPaymentTypeService paymentTypeService, IMapper mapper)
            {
                _paymentTypeService = paymentTypeService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<PaymentTypeToReturnDto>> Handle(GetPaymentTypesQuery request, CancellationToken cancellationToken)
            {
                var paymentType = await _paymentTypeService.GetPaymentTypes();
                return _mapper.Map<IEnumerable<PaymentTypeToReturnDto>>(paymentType);
            }
        }
    }
}

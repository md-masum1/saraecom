﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.PaymentTypes
{
    public class PaymentTypeToReturnDto : IMapFrom<PaymentType>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ImagePath { get; set; }
        public byte DisplayOrder { get; set; }
    }
}

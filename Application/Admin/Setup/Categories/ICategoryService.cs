﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Categories
{
    public interface ICategoryService : IDisposable
    {
        Task<IList<Category>> GetCategories();

        Task<Result> CreateCategory(Category category);

        Task<Result> CreateSubCategory(SubCategory subCategory);

        Task<Result> DeleteCategory(int id);

        Task<Result> DeleteSubCategory(int id);
    }
}

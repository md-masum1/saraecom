﻿using System;
using System.Collections.Generic;
using System.Text;
using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Categories
{
    public class CategoryToReturnDto : IMapFrom<Category>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SubCategoryToReturnDto> SubCategories { get; set; }
    }

    public class SubCategoryToReturnDto : IMapFrom<SubCategory>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

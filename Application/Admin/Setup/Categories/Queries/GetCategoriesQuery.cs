﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Categories.Queries
{
    public class GetCategoriesQuery : IRequest<IList<CategoryToReturnDto>>
    {
        public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, IList<CategoryToReturnDto>>
        {
            private readonly ICategoryService _categoryService;
            private readonly IMapper _mapper;

            public GetCategoriesQueryHandler(ICategoryService categoryService, IMapper mapper)
            {
                _categoryService = categoryService;
                _mapper = mapper;
            }
            public async Task<IList<CategoryToReturnDto>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
            {
                var categories = await _categoryService.GetCategories();

                return _mapper.Map<IList<CategoryToReturnDto>>(categories);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;

namespace Application.Admin.Setup.Categories.Commands
{
    public class CreateCategoryCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, Result>
        {
            private readonly ICategoryService _categoryService;

            public CreateCategoryCommandHandler(ICategoryService categoryService)
            {
                _categoryService = categoryService;
            }
            public async Task<Result> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
            {
                var category = new Category
                {
                    Id = request.Id,
                    Name = request.Name.Trim()
                };

                return await _categoryService.CreateCategory(category);
            }
        }
    }
}

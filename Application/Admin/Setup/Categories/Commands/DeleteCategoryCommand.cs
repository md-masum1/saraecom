﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Categories.Commands
{
    public class DeleteCategoryCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, Result>
        {
            private readonly ICategoryService _categoryService;

            public DeleteCategoryCommandHandler(ICategoryService categoryService)
            {
                _categoryService = categoryService;
            }
            public async Task<Result> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
            {
                return await _categoryService.DeleteCategory(request.Id);
            }
        }
    }
}

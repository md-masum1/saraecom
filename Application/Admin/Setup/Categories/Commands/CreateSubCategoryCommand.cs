﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;

namespace Application.Admin.Setup.Categories.Commands
{
    public class CreateSubCategoryCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public class CreateSubCategoryCommandHandler : IRequestHandler<CreateSubCategoryCommand, Result>
        {
            private readonly ICategoryService _categoryService;

            public CreateSubCategoryCommandHandler(ICategoryService categoryService)
            {
                _categoryService = categoryService;
            }
            public async Task<Result> Handle(CreateSubCategoryCommand request, CancellationToken cancellationToken)
            {
                var subCategory = new SubCategory
                {
                    Id = request.Id,
                    Name = request.Name.Trim(),
                    CategoryId = request.CategoryId
                };

                return await _categoryService.CreateSubCategory(subCategory);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Categories.Commands
{
    public class DeleteSubCategoryCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public class DeleteSubCategoryCommandHandler : IRequestHandler<DeleteSubCategoryCommand, Result>
        {
            private readonly ICategoryService _categoryService;

            public DeleteSubCategoryCommandHandler(ICategoryService categoryService)
            {
                _categoryService = categoryService;
            }
            public async Task<Result> Handle(DeleteSubCategoryCommand request, CancellationToken cancellationToken)
            {
                return await _categoryService.DeleteSubCategory(request.Id);
            }
        }
    }
}

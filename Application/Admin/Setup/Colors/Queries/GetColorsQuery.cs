﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Setup.Colors.Queries
{
    public class GetColorsQuery : IRequest<IList<ColorToReturnDto>>
    {
        public class GetColorsQueryHandler : IRequestHandler<GetColorsQuery, IList<ColorToReturnDto>>
        {
            private readonly IColorService _colorService;
            private readonly IMapper _mapper;

            public GetColorsQueryHandler(IColorService colorService, IMapper mapper)
            {
                _colorService = colorService;
                _mapper = mapper;
            }
            public async Task<IList<ColorToReturnDto>> Handle(GetColorsQuery request, CancellationToken cancellationToken)
            {
                var colors = await _colorService.GetColors();

                return _mapper.Map<IList<ColorToReturnDto>>(colors);
            }
        }
    }
}

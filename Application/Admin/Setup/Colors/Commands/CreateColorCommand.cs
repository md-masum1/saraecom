﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;
using MediatR;

namespace Application.Admin.Setup.Colors.Commands
{
    public class CreateColorCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public class CreateColorCommandHandler : IRequestHandler<CreateColorCommand, Result>
        {
            private readonly IColorService _colorService;

            public CreateColorCommandHandler(IColorService colorService)
            {
                _colorService = colorService;
            }
            public async Task<Result> Handle(CreateColorCommand request, CancellationToken cancellationToken)
            {
                var color = new Color
                {
                    Id = request.Id,
                    Name = request.Name.Trim(),
                    ColorCode = request.ColorCode.Trim()
                };

                return await _colorService.CreateColor(color);
            }
        }
    }
}

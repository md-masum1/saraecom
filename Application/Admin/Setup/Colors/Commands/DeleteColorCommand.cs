﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Setup.Colors.Commands
{
    public class DeleteColorCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteColorCommandHandler : IRequestHandler<DeleteColorCommand, Result>
        {
            private readonly IColorService _colorService;

            public DeleteColorCommandHandler(IColorService colorService)
            {
                _colorService = colorService;
            }
            public async Task<Result> Handle(DeleteColorCommand request, CancellationToken cancellationToken)
            {
                return await _colorService.DeleteColor(request.Id);
            }
        }
    }
}

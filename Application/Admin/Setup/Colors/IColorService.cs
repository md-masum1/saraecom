﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Colors
{
    public interface IColorService : IDisposable
    {
        Task<IList<Color>> GetColors();

        Task<Result> CreateColor(Color color);

        Task<Result> DeleteColor(int id);
    }
}

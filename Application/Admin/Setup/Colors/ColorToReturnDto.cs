﻿using System;
using System.Collections.Generic;
using System.Text;
using Application._Common.Mappings;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Setup.Colors
{
    public class ColorToReturnDto : IMapFrom<Color>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
    }
}

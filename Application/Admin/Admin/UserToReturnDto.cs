﻿namespace Application.Admin.Admin
{
    public class UserToReturnDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string EmployeeId { get; set; }
    }
}

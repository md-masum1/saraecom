﻿using Application._Common.Mappings;
using Application.Admin.Admin.Commands;

namespace Application.Admin.Admin
{
    public class RoleEditDto : IMapFrom<EditRolesCommand>
    {
        public string[] RoleNames { get; set; }
    }
}

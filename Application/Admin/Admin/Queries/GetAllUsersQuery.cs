﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Paging;
using AutoMapper;
using MediatR;

namespace Application.Admin.Admin.Queries
{
    public class GetAllUsersQuery : IRequest<PagedList<UserToReturnDto>>
    {
        public UserParams UserParams { get; set; }
        public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, PagedList<UserToReturnDto>>
        {
            private readonly IAdminService _adminService;
            private readonly IMapper _mapper;

            public GetAllUsersQueryHandler(IAdminService adminService, IMapper mapper)
            {
                _adminService = adminService;
                _mapper = mapper;
            }
            public async Task<PagedList<UserToReturnDto>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
            {
                return await _adminService.GetAllUsersAsync(request.UserParams);
            }
        }
    }
}

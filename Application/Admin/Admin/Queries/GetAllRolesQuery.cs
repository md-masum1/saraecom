﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Application.Admin.Admin.Queries
{
    public class GetAllRolesQuery : IRequest<object>
    {
        public class GetAllRolesQueryHandler : IRequestHandler<GetAllRolesQuery, object>
        {
            private readonly IAdminService _adminService;
            public GetAllRolesQueryHandler(IAdminService adminService)
            {
                _adminService = adminService;
            }
            public async Task<object> Handle(GetAllRolesQuery request, CancellationToken cancellationToken)
            {
                return await _adminService.GetAllRolesAsync();
            }
        }
    }
}

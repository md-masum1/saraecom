﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Application.Admin.Admin.Queries
{
    public class GetUserWithRolesQuery : IRequest<object>
    {
        public class GetUserWithRolesQueryHandler : IRequestHandler<GetUserWithRolesQuery, object>
        {
            private readonly IAdminService _adminService;
            public GetUserWithRolesQueryHandler(IAdminService adminService)
            {
                _adminService = adminService;
            }
            public async Task<object> Handle(GetUserWithRolesQuery request, CancellationToken cancellationToken)
            {
                return await _adminService.GetUserWithRolesAsync();
            }
        }
    }
}

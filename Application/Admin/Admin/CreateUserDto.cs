﻿using System;
using Application._Common.Mappings;
using Application.Admin.Admin.Commands;

namespace Application.Admin.Admin
{
    public class CreateUserDto : IMapFrom<CreateUserCommand>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string EmployeeId { get; set; }
        public string Password { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}

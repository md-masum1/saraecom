﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application._Common.Paging;

namespace Application.Admin.Admin
{
    public interface IAdminService : IDisposable
    {
        Task<string> GetUserNameAsync(int userId);
        Task<PagedList<UserToReturnDto>> GetAllUsersAsync(UserParams userParams);
        Task<UserToReturnDto> GetUserAsync(int userId);
        Task<(Result Result, int UserId)> CreateUserAsync(CreateUserDto createUserDto);
        Task<Result> DeleteUserAsync(int userId);
        Task<object> GetUserWithRolesAsync();
        Task<object> GetAllRolesAsync();
        Task<(Result Result, IList<string> roles)> EditRolesAsync(string userName, RoleEditDto roleEditDto);
    }
}

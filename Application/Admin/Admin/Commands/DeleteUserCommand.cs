﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Admin.Commands
{
    public class DeleteUserCommand : IRequest<Result>
    {
        [Required]
        public int UserId { get; set; }

        public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, Result>
        {
            private readonly IAdminService _adminService;

            public DeleteUserCommandHandler(IAdminService adminService)
            {
                _adminService = adminService;
            }
            public async Task<Result> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
            {
                return await _adminService.DeleteUserAsync(request.UserId);
            }
        }
    }
}

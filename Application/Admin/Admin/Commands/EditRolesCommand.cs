﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Admin.Commands
{
    public class EditRolesCommand : IRequest<(Result result, IList<string> roles)>
    {
        public string UserName { get; set; }
        public string[] RoleNames { get; set; }

        public class EditRolesCommandHandler : IRequestHandler<EditRolesCommand, (Result result, IList<string> roles)>
        {
            private readonly IAdminService _adminService;

            public EditRolesCommandHandler(IAdminService adminService)
            {
                _adminService = adminService;
            }
            public async Task<(Result result, IList<string> roles)> Handle(EditRolesCommand request, CancellationToken cancellationToken)
            {
                var roleList = new RoleEditDto
                {
                    RoleNames = request.RoleNames
                };
                var result = await _adminService.EditRolesAsync(request.UserName, roleList);

                return result;
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using Application._Common.Models;
using AutoMapper;
using MediatR;

namespace Application.Admin.Admin.Commands
{
    public class CreateUserCommand : IRequest<(Result result, UserToReturnDto user)>
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string EmployeeId { get; set; }
        [Required]
        public string Password { get; set; }

        public class EditRolesCommandHandler : IRequestHandler<CreateUserCommand, (Result result, UserToReturnDto user)>
        {
            private readonly IAdminService _adminService;
            private readonly IDateTime _dateTime;
            private readonly IMapper _mapper;
            private readonly ICurrentUserService _currentUserService;

            public EditRolesCommandHandler(IAdminService adminService, IDateTime dateTime, IMapper mapper, ICurrentUserService currentUserService)
            {
                _adminService = adminService;
                _dateTime = dateTime;
                _mapper = mapper;
                _currentUserService = currentUserService;
            }
            public async Task<(Result result, UserToReturnDto user)> Handle(CreateUserCommand request, CancellationToken cancellationToken)
            {
                var user = _mapper.Map<CreateUserDto>(request);
                user.CreateBy = _currentUserService.EmployeeId;
                user.CreateDate = _dateTime.Now;

                var result = await _adminService.CreateUserAsync(user);

                if (result.Result.Succeeded)
                {
                    var userToReturn = await _adminService.GetUserAsync(result.UserId);
                    return (result.Result, userToReturn);
                }

                return (result.Result, null);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application._Common.Models;
using Application._Common.Paging;
using Domain.Entities.Admin.Product;

namespace Application.Admin.Products
{
    public interface IGoodsService : IDisposable
    {
        Task<PagedList<Product>> GetProduct(ProductParams productParams);
        Task<Product> GetProductByStyle(string style);


        Task<(Result result, int productId)> SaveWarehouseProduct(Product products);

        Task<IList<ProductImage>> GetProductImage(int itemId);

        Task<Result> UpdateProduct(Product product);

        Task<Result> UpdateProductItem(ProductItem productItem);



        Task<Result> CreateProduct(Product product);

        Task<Result> UploadProductImage(ProductImage productImage);
        Task<Result> DeleteProductImage(int itemId, int imageId);

        Task<IList<ProductColor>> GetProductColor(int productId);
        Task<Result> AddProductColor(ProductColor productColor);
        Task<Result> DeleteProductColor(int colorId, int productId);

        Task<IList<ProductSize>> GetProductSize(int productId);
        Task<Result> AddProductSize(ProductSize productSize);
        Task<Result> DeleteProductSize(int id);
    }
}

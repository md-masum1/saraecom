﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Product;

namespace Application.Admin.Products
{
    public class PhotoToReturnDto : IMapFrom<ProductImage>
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public int ProductId { get; set; }
    }
}

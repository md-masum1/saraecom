﻿using System.Collections.Generic;
using Application._Common.Mappings;
using Domain.Entities.Admin.Product;
using Domain.Entities.Admin.Setup;

namespace Application.Admin.Products
{
    public class ProductToReturnDto : IMapFrom<Product>
    {
        public int Id { get; set; }
        public int? WhProductId { get; set; }
        public string Name { get; set; }
        public string ProductStyle { get; set; }
        public bool IsActive { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Fabric { get; set; }

        public CategoryToReturnDto Category { get; set; }
        public int CategoryId { get; set; }

        public SubCategoryToReturnDto SubCategory { get; set; }
        public int SubCategoryId { get; set; }

        public ICollection<ProductColor> ProductColors { get; set; }
        public ICollection<ProductSize> ProductSizes { get; set; }
        public ICollection<ProductTag> ProductTags { get; set; }
        public ICollection<PhotoToReturnDto> Pictures { get; set; }

        public bool? New { get; set; }
        public bool? Sale { get; set; }
        public float? Price { get; set; }
        public float? SalePrice { get; set; }
        public float? Discount { get; set; }
        public byte? Stock { get; set; }

        public virtual ICollection<ProductItem> ProductItem { get; set; }
    }

    public class CategoryToReturnDto : IMapFrom<Category>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SubCategoryToReturnDto : IMapFrom<SubCategory>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;

namespace Application.Admin.Products.Commands
{
    public class GetProductFromPosCommand : IRequest<Result>
    {
        public class GetProductFromPosCommandHandler : IRequestHandler<GetProductFromPosCommand, Result>
        {
            private readonly IPosService _posService;
            private readonly IGoodsService _goodsService;

            public GetProductFromPosCommandHandler(IPosService posService, IGoodsService goodsService)
            {
                _posService = posService;
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(GetProductFromPosCommand request, CancellationToken cancellationToken)
            {
                await _posService.LoadWarehouseProduct();

                var result = await GetProduct();

                return result;
            }

            private async Task<Result> GetProduct()
            {
                var posProduct = await _posService.GetProductsFromWarehouse();

                if (posProduct.Any())
                {
                    await SaveProduct(posProduct);
                }

                return Result.Success();
            }

            private async Task SaveProduct(IList<Product> products)
            {
                foreach (var product in products)
                {
                    var result = await _goodsService.SaveWarehouseProduct(product);

                    if (result.result.Succeeded)
                    {
                        if (!await _posService.UpdateWarehouseProduct(result.productId))
                        {
                            throw new Exception("can't update warehouse product");
                        }
                    }
                    else
                    {
                        throw new Exception("can't save warehouse product");
                    }
                }

                await GetProduct();
            }
        }
    }
}

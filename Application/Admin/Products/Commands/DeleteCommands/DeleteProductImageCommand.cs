﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Products.Commands.DeleteCommands
{
    public class DeleteProductImageCommand : IRequest<Result>
    {
        public int ProductId { get; set; }
        public int ImageId { get; set; }

        public class DeleteProductImageCommandHandler : IRequestHandler<DeleteProductImageCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public DeleteProductImageCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(DeleteProductImageCommand request, CancellationToken cancellationToken)
            {
                return await _goodsService.DeleteProductImage(request.ProductId, request.ImageId);
            }
        }
    }
}

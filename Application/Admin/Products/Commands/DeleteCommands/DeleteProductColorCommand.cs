﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Products.Commands.DeleteCommands
{
    public class DeleteProductColorCommand : IRequest<Result>
    {
        public int ColorId { get; set; }
        public int ProductId { get; set; }
        public class DeleteProductColorCommandHandler : IRequestHandler<DeleteProductColorCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public DeleteProductColorCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(DeleteProductColorCommand request, CancellationToken cancellationToken)
            {
                return await _goodsService.DeleteProductColor(request.ColorId, request.ProductId);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using MediatR;

namespace Application.Admin.Products.Commands.DeleteCommands
{
    public class DeleteProductSizeCommand : IRequest<Result>
    {
        public int Id { get; set; }

        public class DeleteProductSizeCommandHandler : IRequestHandler<DeleteProductSizeCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public DeleteProductSizeCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }

            public async Task<Result> Handle(DeleteProductSizeCommand request, CancellationToken cancellationToken)
            {
                return await _goodsService.DeleteProductSize(request.Id);
            }
        }
    }
}

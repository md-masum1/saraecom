﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Products.Commands.CreateCommands
{
    public class UploadProductImageCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public IFormFile File { get; set; }

        public class UploadProductImageCommandHandler : IRequestHandler<UploadProductImageCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public UploadProductImageCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(UploadProductImageCommand request, CancellationToken cancellationToken)
            {
                var productImage = new ProductImage
                {
                    ProductId = request.Id,
                    ImageFile = request.File
                };
                return await _goodsService.UploadProductImage(productImage);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;

namespace Application.Admin.Products.Commands.CreateCommands
{
    public class AddProductSizeCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int? Stock { get; set; }
        public float? Price { get; set; }
        public int SizeId { get; set; }
        public int ProductId { get; set; }
        public int? ColorId { get; set; }

        public class AddProductSizeCommandHandler : IRequestHandler<AddProductSizeCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public AddProductSizeCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(AddProductSizeCommand request, CancellationToken cancellationToken)
            {
                var productSize = new ProductSize
                {
                    Id = request.Id,
                    Stock = request.Stock,
                    Price = request.Price,
                    SizeId = request.SizeId,
                    ProductId = request.ProductId,
                    ColorId = request.ColorId
                };

                return await _goodsService.AddProductSize(productSize);
            }
        }
    }
}

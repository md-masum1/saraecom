﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;

namespace Application.Admin.Products.Commands.CreateCommands
{
    public class CreateProductCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int? WhProductId { get; set; }
        public string Name { get; set; }
        public string ProductStyle { get; set; }
        public bool IsActive { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Fabric { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public bool? New { get; set; }
        public bool? Sale { get; set; }
        public float? Price { get; set; }
        public float? SalePrice { get; set; }
        public float? Discount { get; set; }
        public byte? Stock { get; set; }

        public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public CreateProductCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(CreateProductCommand request, CancellationToken cancellationToken)
            {
                var product = new Product
                {
                    Id = request.Id,
                    WhProductId = request.WhProductId,
                    Name = request.Name.Trim(),
                    ProductStyle = request.ProductStyle.Trim(),
                    IsActive = request.IsActive,
                    ShortDescription = request.ShortDescription.Trim(),
                    LongDescription = request.LongDescription.Trim(),
                    Fabric = request.Fabric.Trim(),
                    CategoryId = request.CategoryId,
                    SubCategoryId = request.SubCategoryId,
                    New = request.New,
                    Sale = request.Sale,
                    Price = request.Price,
                    SalePrice = request.SalePrice,
                    Discount = request.Discount,
                    Stock = request.Stock
                };

                return await _goodsService.CreateProduct(product);

            }
        }
    }
}

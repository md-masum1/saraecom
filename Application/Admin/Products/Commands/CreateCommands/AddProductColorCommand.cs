﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Products.Commands.CreateCommands
{
    public class AddProductColorCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ColorId { get; set; }
        public IFormFile ImageFile { get; set; }

        public class AddProductColorCommandHandler : IRequestHandler<AddProductColorCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public AddProductColorCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(AddProductColorCommand request, CancellationToken cancellationToken)
            {
                var productColor = new ProductColor
                {
                    Id = request.Id,
                    ProductId = request.ProductId,
                    ColorId = request.ColorId,
                    ImageFile = request.ImageFile
                };

                return await _goodsService.AddProductColor(productColor);
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Products.Commands
{
    public class UpdateProductItemCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public string ColorName { get; set; }
        [Required]
        public string ColorCode { get; set; }
        [Required]
        public string SizeName { get; set; }
        [Required]
        public string FabricName { get; set; }
        public string Images { get; set; }
        public IFormFile ImageFile { get; set; }

        public byte? Stock { get; set; } = 0;

        public class UpdateProductItemCommandHandler : IRequestHandler<UpdateProductItemCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public UpdateProductItemCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(UpdateProductItemCommand request, CancellationToken cancellationToken)
            {
                var productItem = new ProductItem
                {
                    Id = request.Id,
                    ProductId = request.ProductId,
                    IsActive = request.IsActive,
                    ColorName = request.ColorName,
                    ColorCode = request.ColorCode,
                    SizeName = request.SizeName,
                    FabricName = request.FabricName,
                    Images = request.Images,
                    ImageFile = request.ImageFile,
                    Stock = request.Stock
                };

                return await _goodsService.UpdateProductItem(productItem);
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Admin.Product;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Application.Admin.Products.Commands
{
    public class UpdateProductCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public int WhProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductStyle { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string LongDescription { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }

        public bool? New { get; set; } = false;
        public bool? Sale { get; set; } = false;
        public float? Price { get; set; } = 0;
        public float? SalePrice { get; set; } = 0;
        public float? Discount { get; set; } = 0;
        public byte? Stock { get; set; } = 0;

        public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Result>
        {
            private readonly IGoodsService _goodsService;

            public UpdateProductCommandHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<Result> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
            {
                var product = new Product
                {
                    Id = request.Id,
                    WhProductId = request.WhProductId,
                    Name = request.ProductName,
                    ProductStyle = request.ProductStyle,
                    IsActive = request.IsActive,
                    ShortDescription = request.ShortDescription,
                    LongDescription = request.LongDescription,

                    New = request.New,
                    Sale = request.Sale,
                    Price = request.Price,
                    SalePrice = request.SalePrice,
                    Discount = request.Discount,
                    Stock = request.Stock
                };

                return await _goodsService.UpdateProduct(product);
            }
        }
    }
}

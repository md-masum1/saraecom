﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Product;

namespace Application.Admin.Products
{
    public class ProductSizeToReturnDto : IMapFrom<ProductSize>
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? Stock { get; set; }
        public float? Price { get; set; }

        public int? ColorId { get; set; }
        public string ColorName { get; set; }

        public int SizeId { get; set; }
        public string SizeName { get; set; }
    }
}

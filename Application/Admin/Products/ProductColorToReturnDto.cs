﻿using Application._Common.Mappings;
using Domain.Entities.Admin.Product;

namespace Application.Admin.Products
{
    public class ProductColorToReturnDto : IMapFrom<ProductColor>
    {
        public int Id { get; set; }
        public string Images { get; set; }
        public int ColorId { get; set; }
        public string ColorName { get; set; }
        public int ProductId { get; set; }
    }
}

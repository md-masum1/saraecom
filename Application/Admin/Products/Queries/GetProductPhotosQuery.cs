﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Products.Queries
{
    public class GetProductPhotosQuery : IRequest<IList<PhotoToReturnDto>>
    {
        public int ProductId { get; set; }

        public class GetProductPhotosQueryHandler : IRequestHandler<GetProductPhotosQuery, IList<PhotoToReturnDto>>
        {
            private readonly IGoodsService _goodsService;
            private readonly IMapper _mapper;

            public GetProductPhotosQueryHandler(IGoodsService goodsService, IMapper mapper)
            {
                _goodsService = goodsService;
                _mapper = mapper;
            }
            public async Task<IList<PhotoToReturnDto>> Handle(GetProductPhotosQuery request, CancellationToken cancellationToken)
            {
                var productImage = await _goodsService.GetProductImage(request.ProductId);

                return _mapper.Map<IList<PhotoToReturnDto>>(productImage);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Products.Queries
{
    public class GetProductSizeQuery : IRequest<IList<ProductSizeToReturnDto>>
    {
        public int ProductId { get; set; }

        public class GetProductSizeQueryHandler : IRequestHandler<GetProductSizeQuery, IList<ProductSizeToReturnDto>>
        {
            private readonly IGoodsService _goodsService;
            private readonly IMapper _mapper;

            public GetProductSizeQueryHandler(IGoodsService goodsService, IMapper mapper)
            {
                _goodsService = goodsService;
                _mapper = mapper;
            }
            public async Task<IList<ProductSizeToReturnDto>> Handle(GetProductSizeQuery request, CancellationToken cancellationToken)
            {
                var productSizes = await _goodsService.GetProductSize(request.ProductId);

                var productSizeList = _mapper.Map<IList<ProductSizeToReturnDto>>(productSizes);

                return productSizeList.OrderBy(c => c.ColorName).ThenBy(s => s.SizeName).ToList();
            }
        }
    }
}

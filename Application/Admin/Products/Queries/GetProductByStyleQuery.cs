﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Admin.Products.Queries
{
    public class GetProductByStyleQuery : IRequest<ProductToReturnDto>
    {
        public string Style { get; set; }
        public class GetProductByStyleQueryHandler : IRequestHandler<GetProductByStyleQuery, ProductToReturnDto>
        {
            private readonly IGoodsService _goodsService;
            private readonly IMapper _mapper;

            public GetProductByStyleQueryHandler(IGoodsService goodsService, IMapper mapper)
            {
                _goodsService = goodsService;
                _mapper = mapper;
            }
            public async Task<ProductToReturnDto> Handle(GetProductByStyleQuery request, CancellationToken cancellationToken)
            {
                var product = await _goodsService.GetProductByStyle(request.Style);

                return _mapper.Map<ProductToReturnDto>(product);
            }
        }
    }
}

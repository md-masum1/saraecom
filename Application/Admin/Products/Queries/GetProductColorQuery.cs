﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Entities.Admin.Product;
using MediatR;

namespace Application.Admin.Products.Queries
{
    public class GetProductColorQuery : IRequest<IList<ProductColorToReturnDto>>
    {
        public int ProductId { get; set; }
        public class GetProductColorQueryHandler : IRequestHandler<GetProductColorQuery, IList<ProductColorToReturnDto>>
        {
            private readonly IGoodsService _goodsService;
            private readonly IMapper _mapper;

            public GetProductColorQueryHandler(IGoodsService goodsService, IMapper mapper)
            {
                _goodsService = goodsService;
                _mapper = mapper;
            }
            public async Task<IList<ProductColorToReturnDto>> Handle(GetProductColorQuery request, CancellationToken cancellationToken)
            {
                var productColor = await _goodsService.GetProductColor(request.ProductId);

                return _mapper.Map<IList<ProductColorToReturnDto>>(productColor);
            }
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Paging;
using Domain.Entities.Admin.Product;
using MediatR;

namespace Application.Admin.Products.Queries
{
    public class GetProductQuery : IRequest<PagedList<Product>>
    {
        public ProductParams ProductParams { get; set; }

        public class GetProductQueryHandler : IRequestHandler<GetProductQuery, PagedList<Product>>
        {
            private readonly IGoodsService _goodsService;

            public GetProductQueryHandler(IGoodsService goodsService)
            {
                _goodsService = goodsService;
            }
            public async Task<PagedList<Product>> Handle(GetProductQuery request, CancellationToken cancellationToken)
            {
                return await _goodsService.GetProduct(request.ProductParams);
            }
        }
    }
}

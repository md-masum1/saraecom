﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application._Common.Mappings;
using Application.Ecom.Home;
using AutoMapper;
using Domain.Entities.Admin.Product;

namespace Application.Ecom.EcomProduct
{
    public class EcomProductToReturnDto : IMapFrom<Product>
    {
        public int Id { get; set; }
        public int? WhProductId { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public float? Price { get; set; }
        public float? SalePrice { get; set; }
        public float? Discount { get; set; }
        public string[] Pictures { get; set; }
        public string ShortDetails { get; set; }
        public string Description { get; set; }
        public byte Stock { get; set; }
        public bool? New { get; set; }
        public bool? Sale { get; set; }
        public string Category { get; set; }
        public string[] Colors { get; set; }
        public string[] Size { get; set; }
        public string[] Tags { get; set; }
        public List<Variants> Variants { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Product, EcomProductToReturnDto>()
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.Style, opts => opts.MapFrom(s => s.ProductStyle))
                .ForMember(s => s.Pictures, opts => opts.MapFrom(s => s.Pictures.Select(c => c.Image).ToArray()))
                .ForMember(s => s.ShortDetails, opts => opts.MapFrom(s => s.ShortDescription))
                .ForMember(s => s.Description, opts => opts.MapFrom(s => s.LongDescription))
                .ForMember(s => s.Colors,
                    opts => opts.MapFrom(s => s.ProductItem.GroupBy(c => c.ColorName).Select(c => c.Key).ToArray()))
                .ForMember(s => s.Size,
                    opts => opts.MapFrom(s => s.ProductItem.GroupBy(c => c.SizeName).Select(c => c.Key).ToArray()))
                .ForMember(s => s.Tags, opts => opts.MapFrom(s => s.ProductTags.ToArray()))
                .ForMember(s => s.Variants, opts => opts.MapFrom(s => s.ProductItem));
        }
    }

    public class Variants : IMapFrom<ProductItem>
    {
        public int Id { get; set; }
        public string Color { get; set; }
        public string ColorCode { get; set; }
        public string Images { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ProductItem, Variants>()
                .ForMember(d => d.Color, opts => opts.MapFrom(s => s.ColorName));
        }
    }
}

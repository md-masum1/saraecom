﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Application._Common.Paging;
using Domain.Entities.Admin.Product;

namespace Application.Ecom.EcomProduct
{
    public interface IEcomProductService : IDisposable
    {
        Task<PagedList<Product>> GetProducts(EcomProductParams productParams);
    }
}

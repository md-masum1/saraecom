﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Ecom.EcomProduct
{
    public class EcomProductParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int _pageSize = 20;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
    }
}

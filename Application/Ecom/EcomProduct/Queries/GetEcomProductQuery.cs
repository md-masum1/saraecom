﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Paging;
using AutoMapper;
using MediatR;
using MoreLinq;

namespace Application.Ecom.EcomProduct.Queries
{
    public class GetEcomProductQuery : IRequest<PagedList<EcomProductToReturnDto>>
    {
        public EcomProductParams ProductParams { get; set; }
        public class GetEcomProductQueryHandler : IRequestHandler<GetEcomProductQuery, PagedList<EcomProductToReturnDto>>
        {
            private readonly IEcomProductService _ecomProductService;
            private readonly IMapper _mapper;

            public GetEcomProductQueryHandler(IEcomProductService ecomProductService, IMapper mapper)
            {
                _ecomProductService = ecomProductService;
                _mapper = mapper;
            }
            public async Task<PagedList<EcomProductToReturnDto>> Handle(GetEcomProductQuery request, CancellationToken cancellationToken)
            {
                var product = await _ecomProductService.GetProducts(request.ProductParams);

                var result = _mapper.Map<List<EcomProductToReturnDto>>(product);

                foreach (var item in result)
                {
                    var variant = item.Variants;

                    variant = variant.DistinctBy(c => c.Color).ToList();

                    item.Variants = variant;
                }

                return new PagedList<EcomProductToReturnDto>(result, product.TotalCount, product.CurrentPage, product.PageSize);
            }
        }
    }
}

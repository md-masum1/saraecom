﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Ecom.Home.Queries
{
    public class GetSlidersQuery : IRequest<IEnumerable<SliderForHomeDto>>
    {
        public class GetSlidersQueryHandler : IRequestHandler<GetSlidersQuery, IEnumerable<SliderForHomeDto>>
        {
            private readonly IHomeService _homeService;
            private readonly IMapper _mapper;

            public GetSlidersQueryHandler(IHomeService homeService, IMapper mapper)
            {
                _homeService = homeService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<SliderForHomeDto>> Handle(GetSlidersQuery request, CancellationToken cancellationToken)
            {
                var result = await _homeService.GetSliders();

                return _mapper.Map<IEnumerable<SliderForHomeDto>>(result);
            }
        }
    }
}

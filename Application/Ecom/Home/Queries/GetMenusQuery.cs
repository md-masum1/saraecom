﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Ecom.Home.Queries
{
    public class GetMenusQuery : IRequest<IEnumerable<MenuForHomeDto>>
    {
        public class GetMenusQueryHandler : IRequestHandler<GetMenusQuery, IEnumerable<MenuForHomeDto>>
        {
            private readonly IHomeService _homeService;
            private readonly IMapper _mapper;

            public GetMenusQueryHandler(IHomeService homeService, IMapper mapper)
            {
                _homeService = homeService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<MenuForHomeDto>> Handle(GetMenusQuery request, CancellationToken cancellationToken)
            {
                var result = await _homeService.GetMenus();

                return _mapper.Map<IEnumerable<MenuForHomeDto>>(result);
            }
        }
    }
}

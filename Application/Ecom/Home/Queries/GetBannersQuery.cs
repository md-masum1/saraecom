﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Application.Ecom.Home.Queries
{
    public class GetBannersQuery : IRequest<IEnumerable<BannerForHomeDto>>
    {
        public class GetBannersQueryHandler : IRequestHandler<GetBannersQuery, IEnumerable<BannerForHomeDto>>
        {
            private readonly IHomeService _homeService;
            private readonly IMapper _mapper;

            public GetBannersQueryHandler(IHomeService homeService, IMapper mapper)
            {
                _homeService = homeService;
                _mapper = mapper;
            }
            public async Task<IEnumerable<BannerForHomeDto>> Handle(GetBannersQuery request, CancellationToken cancellationToken)
            {
                var result = await _homeService.GetBanners();

                return _mapper.Map<IEnumerable<BannerForHomeDto>>(result);
            }
        }
    }
}

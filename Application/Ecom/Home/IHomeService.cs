﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.Admin.Setup;
using Domain.Entities.Admin.Setup.Menus;

namespace Application.Ecom.Home
{
    public interface IHomeService : IDisposable
    {
        Task<IList<Menu>> GetMenus();
        Task<IList<HomePageSlider>> GetSliders();
        Task<IList<HomePageBanner>> GetBanners();
    }
}

﻿using System.Collections.Generic;
using Application._Common.Mappings;
using AutoMapper;
using Domain.Entities.Admin.Setup.Menus;

namespace Application.Ecom.Home
{
    public class MenuForHomeDto : IMapFrom<Menu>
    {
        public MenuForHomeDto()
        {
            Children = new List<SubMenuForHomeDto>();
        }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public ICollection<SubMenuForHomeDto> Children { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Menu, MenuForHomeDto>()
                .ForMember(d => d.Children, 
                    memberOptions => memberOptions.MapFrom(s => s.SubMenus));
        }
    }

    public class SubMenuForHomeDto : IMapFrom<SubMenu>
    {
        public SubMenuForHomeDto()
        {
            Children = new List<SubSubMenuForHomeDto>();
        }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public int MenuId { get; set; }
        public ICollection<SubSubMenuForHomeDto> Children { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SubMenu, SubMenuForHomeDto>()
                .ForMember(d => d.Children,
                    memberOptions => memberOptions.MapFrom(s => s.SubSubMenus));
        }
    }

    public class SubSubMenuForHomeDto : IMapFrom<SubSubMenu>
    {
        public string Path { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public bool MegaMenu { get; set; }
        public string MegaMenuType { get; set; }
        public string Image { get; set; }
        public int SubMenuId { get; set; }
    }
}

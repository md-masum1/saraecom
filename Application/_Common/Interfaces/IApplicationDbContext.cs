﻿using System.Threading;
using System.Threading.Tasks;

namespace Application._Common.Interfaces
{
    public interface IApplicationDbContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}

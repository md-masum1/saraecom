﻿namespace Application._Common.Interfaces
{
    public interface ICurrentUserService
    {
        int UserId { get; }
        string UserName { get; }
        string EmployeeId { get; }
    }
}

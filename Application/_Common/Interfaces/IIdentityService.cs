﻿using System.Threading.Tasks;
using Application._Common.Models;
using Application.Auth;

namespace Application._Common.Interfaces
{
    public interface IIdentityService
    {
        Task<object> Login(UserForLoginDto userForLogin);
        Task<(Result Result, int UserId)> Register(UserForRegisterDto userForRegister);
    }
}

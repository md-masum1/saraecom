﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Models;
using Domain.Entities.Logger;

namespace Application._Common.Interfaces
{
    public interface ILogToDatabaseService
    {
        Task<Result> Save(RequestLoggerEntity loggerEntity, CancellationToken cancellationToken);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities.Admin.Product;

namespace Application._Common.Interfaces
{
    public interface IPosService
    {
        Task<IList<Product>> GetProductsFromWarehouse();
        Task<bool> UpdateWarehouseProduct(int whProductId);

        Task LoadWarehouseProduct();
    }
}

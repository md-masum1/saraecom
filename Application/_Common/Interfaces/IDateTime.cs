﻿using System;

namespace Application._Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}

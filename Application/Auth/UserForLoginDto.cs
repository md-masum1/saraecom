﻿using Application._Common.Mappings;
using Application.Auth.Commands;

namespace Application.Auth
{
    public class UserForLoginDto : IMapFrom<LoginCommand>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

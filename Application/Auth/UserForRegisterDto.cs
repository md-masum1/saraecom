﻿using Application._Common.Mappings;
using Application.Auth.Commands;

namespace Application.Auth
{
    public class UserForRegisterDto : IMapFrom<RegisterCommand>
    {
        public string EmployeeId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
    }
}

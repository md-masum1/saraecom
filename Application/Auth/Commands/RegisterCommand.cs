﻿using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using Application._Common.Models;
using AutoMapper;
using MediatR;

namespace Application.Auth.Commands
{
    public class RegisterCommand : IRequest<Result>
    {
        public string EmployeeId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string PhoneNumber { get; set; }

        public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Result>
        {
            private readonly IIdentityService _identityService;
            private readonly IMapper _mapper;

            public RegisterCommandHandler(IIdentityService identityService, IMapper mapper)
            {
                _identityService = identityService;
                _mapper = mapper;
            }

            public async Task<Result> Handle(RegisterCommand request, CancellationToken cancellationToken)
            {
                var user = _mapper.Map<UserForRegisterDto>(request);

                var result = await _identityService.Register(user);

                return result.Result;
            }
        }
    }
}

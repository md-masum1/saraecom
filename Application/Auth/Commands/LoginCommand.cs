﻿using System.Threading;
using System.Threading.Tasks;
using Application._Common.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Auth.Commands
{
    public class LoginCommand : IRequest<object>
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public class LoginCommandHandler : IRequestHandler<LoginCommand, object>
        {
            private readonly IIdentityService _identityService;
            private readonly IMapper _mapper;

            public LoginCommandHandler(IIdentityService identityService, IMapper mapper)
            {
                _identityService = identityService;
                _mapper = mapper;
            }

            public async Task<object> Handle(LoginCommand request, CancellationToken cancellationToken)
            {
                var user = _mapper.Map<UserForLoginDto>(request);

                return await _identityService.Login(user);
            }
        }
    }
}
